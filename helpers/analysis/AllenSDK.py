from .base import  *

import json
from collections import defaultdict
from allensdk.core.cell_types_cache import CellTypesCache
from allensdk.api.queries.cell_types_api import CellTypesApi
from allensdk.core.cell_types_cache import ReporterStatus as RS
from allensdk.api.queries.biophysical_api import BiophysicalApi
from allensdk.core.nwb_data_set import NwbDataSet
import allensdk.model.biophysical.runner as BiophysicalRunner
import allensdk.model.biophysical.utils as BiophysicalUtils
from allensdk.ephys.extract_cell_features import extract_cell_features
from allensdk.ephys.ephys_extractor import EphysSweepFeatureExtractor, _step_stim_amp
import allensdk.ephys.extract_cell_features as allen_extract_cell_features
import efel
from matplotlib import pyplot as plt
from functools import wraps
import warnings
from itertools import repeat
import multiprocessing as mp

class AllenSDK:

    def __init__(self, input_data_folder, specimen_files_folder, biophys_models_folder):
        self.input_data_folder = input_data_folder
        self.specimen_files_folder = specimen_files_folder
        self.biophys_models_folder = biophys_models_folder


    def get_electro_features(self, verbose=True):
        all_features_efel = []
        all_features_allensdk = []

        specimens_dirs = next(os.walk(self.specimen_files_folder))[1]
        for i, specimen_id in enumerate(specimens_dirs):
            if verbose:
                print("Processing features of specimen {} of {}".format(i, len(specimens_dirs)))

            specimen_dir = os.path.join(self.specimen_files_folder, specimen_id)
            nwb_data_set, sweep_numbers, sweeps_by_type = self.get_nwb_and_sweeps(specimen_dir, is_biophys_model=False)

            features_efel = self.get_efel_features(nwb_data_set, sweep_numbers, show_sweeps_plots=True)
            features_efel["specimen_id"] = specimen_id
            features_allensdk = self.get_allensdk_features(nwb_data_set, sweeps_by_type)
            features_allensdk["specimen_id"] = specimen_id

            all_features_efel.append(features_efel)
            all_features_allensdk.append(features_allensdk)

        df_efel = pd.DataFrame(all_features_efel)
        df_allensdk = pd.DataFrame(all_features_allensdk)

        df_efel.to_csv(os.path.join(self.input_data_folder, "raw_electro_efel.csv"), index=False)
        df_allensdk.to_csv(os.path.join(self.input_data_folder, "raw_electro_allensdk.csv"), index=False)

        return 0

    def get_biophys_electro_features(self, verbose=True):
        """
        http://alleninstitute.github.io/AllenSDK/cell_types.html
        https://github.com/BlueBrain/eFEL
        """
        all_features_efel = []
        all_features_allensdk = []

        biophys_models_dirs = next(os.walk(self.biophys_models_folder))[1]
        for i, model_id in enumerate(biophys_models_dirs):
            if verbose:
                print("Processing features of model {} of {}".format(i, len(biophys_models_dirs)))

            model_dir = os.path.join(self.biophys_models_folder, model_id)
            nwb_data_set, sweep_numbers, sweeps_by_type = self.get_nwb_and_sweeps(model_dir, is_biophys_model=False)

            features_efel = self.get_efel_features(nwb_data_set, sweep_numbers)
            features_allensdk = self.get_allensdk_features(nwb_data_set, sweeps_by_type)

            all_features_efel.append(features_efel)
            all_features_allensdk.append(features_allensdk)

        df_efel = pd.DataFrame(all_features_efel)
        df_allensdk = pd.DataFrame(all_features_allensdk)

        df_efel.to_csv(os.path.join(self.input_data_folder, "raw_electro_efel.csv"), index=False)
        df_allensdk.to_csv(os.path.join(self.input_data_folder, "raw_electro_allensdk.csv"), index=False)

        return 0

    def get_all_biophys_models_electro_features(self):
        """
        http://alleninstitute.github.io/AllenSDK/cell_types.html
        https://github.com/BlueBrain/eFEL
        """
        all_features_efel = []
        all_features_allensdk = []

        biophys_models_dirs = next(os.walk(self.biophys_models_folder))[1]

        args = zip(biophys_models_dirs, range(len(biophys_models_dirs)), repeat(len(biophys_models_dirs)))
        with mp.Pool() as pool:
            for features_efel in pool.starmap(self.get_biophys_model_electro_features, args):
                all_features_efel.append(features_efel)
                # all_features_allensdk.append(features_allensdk)

        df_efel = pd.DataFrame(all_features_efel)
        #df_allensdk = pd.DataFrame(all_features_allensdk)

        df_efel.to_csv(os.path.join(self.input_data_folder, "raw_biophys_models_electro_efel.csv"), index=False)
        #df_allensdk.to_csv(os.path.join(self.input_data_folder, "raw_biophys_model_electro_allensdk.csv"), index=False)

        return 0

    def get_biophys_model_electro_features(self, model_id, i, num_models):
        print("Processing features of model {} of {}".format(i, num_models))

        model_dir = os.path.join(self.biophys_models_folder, model_id)
        model_responses_dir = os.path.join(model_dir, "model_sweeps_responses")
        model_info_file = os.path.join(model_dir, 'model_info.json')
        with open(model_info_file, 'r') as file:
            model_info = json.load(file)
        specimen_id = model_info["specimen_id"]
        features_efel = {}

        for sweep_file in os.listdir(model_responses_dir):
            sweep_name = os.path.splitext(sweep_file)[0]
            sweep_extension = os.path.splitext(sweep_file)[1]
            if sweep_extension == ".txt":  # Valid sweep file
                sweep_file_path = os.path.join(model_responses_dir, sweep_file)
                sweep_type = sweep_name.split('__')[1]
                times, voltages = biophys_io.AllenSDK.read_stimulus_txt(sweep_file_path, show_plots=False)

                feature_sweep_efel = self.get_efel_features_sweep(voltages, times, prefix_features=sweep_type)
                features_efel.update(feature_sweep_efel)
                # features_values_allensdk = self.get_allensdk_features_sweep(voltages, times) #Why it doesn't detect the spikes?

        features_efel["specimen_id"] = specimen_id

        return features_efel

    def get_nwb_and_sweeps(self, model_dir, is_biophys_model=False):

        ephys_sweeps_path = os.path.join(model_dir, "ephys_sweeps.json")
        with open(ephys_sweeps_path) as data_file:
            ephys_sweeps = json.load(data_file)
        if is_biophys_model:
            manifest_path = os.path.join(model_dir, "manifest.json")
            with open(manifest_path) as data_file:
                manifest = json.load(data_file)["manifest"]
            # Get nwb path
            for item in manifest:
                if item["key"] == "stimulus_path":
                    nwb_path = os.path.join(model_dir, item["spec"])
        else:
            nwb_path = os.path.join(model_dir, "ephys_data.nwb")

        nwb_data_set = NwbDataSet(nwb_path)

        # group the sweeps by stimulus
        sweeps_all_by_type = defaultdict(list)
        for sweep in ephys_sweeps:
            sweeps_all_by_type[sweep['stimulus_name']].append(sweep['sweep_number'])
        """
        We only want the 4 sweep types used to create the all-active biophysical models according to the
        all-active biophysical models white paper:
        Short Square, Long Square, Square - 2s Suprathreshold, Ramp

        Although according to the electrophysiology white paper, the 4 sweep types used in all-active biophysical
        models were:
        Long square, Square - 2s Suprathreshold, Square - 0.5ms Subthreshold, Ramp
        """
        sweeps_by_type = {
            # 'Short Square': sweeps_all_by_type['Short Square'], #ShortDC
            'Long Square': sweeps_all_by_type['Long Square'],  # LongDC
            # 'Square - 2s Suprathreshold': sweeps_all_by_type['Square - 2s Suprathreshold'], #LongSupra ?
            # 'Square - 0.5ms Subthreshold': sweeps_all_by_type['Square - 0.5ms Subthreshold'], #LongSupra ?
            # 'Ramp': sweeps_all_by_type['Ramp'], #Ramp
        }
        sweep_numbers = [sweeps for sweeps_type in sweeps_by_type.values() for sweeps in sweeps_type]

        return nwb_data_set, sweep_numbers, sweeps_by_type

    @biophys_utils.ignore_warnings
    def get_efel_features(self, nwb_data_set, sweep_numbers, show_sweeps_plots=False):
        #https://github.com/BlueBrain/eFEL

        features_efel = {
            "mean_frequency": [],
            "ISI_log_slope": [],
            "adaptation_index2": [],
            "Spikecount": [],
            "time_to_first_spike": [],
            "time_to_last_spike": [],
            "AP_width": [],
            "AP_height": [],
            "min_voltage_between_spikes": [],
            "steady_state_voltage_stimend": [],
            "voltage_base": [],
            "voltage_after_stim": [],
        }

        for i, sweep_number in enumerate(sweep_numbers):
            sweep_data = nwb_data_set.get_sweep(sweep_number)

            # spike times are in seconds relative to the start of the sweep
            spike_times = nwb_data_set.get_spike_times(sweep_number)
            if len(spike_times > 1): #Quality control:
                # stimulus is a numpy array in amps
                stimulus = sweep_data['stimulus']

                # response is a numpy array in volts
                response = sweep_data['response']

                # sampling rate is in Hz
                sampling_rate = sweep_data['sampling_rate']

                # start/stop indices that exclude the experimental test pulse (if applicable)
                index_range = sweep_data['index_range']

                stimulus = stimulus[index_range[0]:index_range[1] + 1]  # in A
                response = response[index_range[0]:index_range[1] + 1]  # in V
                stimulus *= 1e12  # to pA
                response *= 1e3  # to mV
                dt = 1.0e3 / sampling_rate
                time = np.arange(0, len(response)) * dt

                if show_sweeps_plots:
                    plt.rcParams['figure.figsize'] = 10, 10
                    fig, axes = plt.subplots(2, 1, sharex=True)
                    axes[0].plot(time, response)
                    axes[1].plot(time, stimulus)
                    axes[1].set_xlabel('i: {};sweep: {}; Time (ms)'.format(i, sweep_number))
                    axes[0].set_ylabel('Response membrane voltage (mV)')
                    axes[1].set_ylabel('Stimulus (pA)')
                    plt.show()

                # --------Extract features per sweep (with efel)--------------:
                feature_values_efel = self.get_efel_features_sweep(response, time)

                for key, value in feature_values_efel.items():
                    if value is not None:
                        features_efel[key].append(value)

        # Mean of all sweeps
        for key, value in features_efel.items():
            if value is not None:
                features_efel[key] = np.mean(value)

        return features_efel

    def get_efel_features_sweep(self, response, time, prefix_features=None):
        features_efel = {
            "mean_frequency": [],
            "ISI_log_slope": [],
            "adaptation_index2": [],
            "time_to_first_spike": [],
            "time_to_last_spike": [],
            "AP_width": [],
            "AP_height": [],
            "min_voltage_between_spikes": [],
            "steady_state_voltage_stimend": [],
            "voltage_base": [],
            "voltage_after_stim": [],
        }


        trace = {'T': time, 'V': response, 'stim_start': [0], 'stim_end': [time[-1]]}
        feature_values_efel = efel.getFeatureValues([trace], features_efel.keys())[0]

        feature_values_efel["Spikecount"] = 0
        if feature_values_efel:
            if "AP_width" in feature_values_efel and feature_values_efel["AP_width"] is not None:
                feature_values_efel["Spikecount"] = len(feature_values_efel["AP_width"])

        features_efel_result = {}
        for key, value in feature_values_efel.items():
            if value is not None:
                if key != "Spikecount":
                    features_efel_result[key] = value.mean()
                else:
                    features_efel_result[key] = value

                if prefix_features is not None:
                    new_feature_name = prefix_features + "__" + key
                    features_efel_result[new_feature_name] = features_efel_result.pop(key)

        return features_efel_result

    def get_allensdk_features_sweep(self, voltages, times):
        i = np.array([0.18] * len(voltages))
        #i *= 1e12  # to pA
        start = times[0]
        end = times[-1]
        ephys_sweep_features = EphysSweepFeatureExtractor(times, voltages, start=start, end=end, filter=None)
        ephys_sweep_features.process_spikes()

        spikes = ephys_sweep_features.spikes()

        features_values_allensdk = ephys_sweep_features.as_dict()
        sweep_feats_keys = ephys_sweep_features.sweep_feature_keys()
        features_values = []
        for key in sweep_feats_keys:
            feat_val = ephys_sweep_features.sweep_feature(key)
            features_values.append(feat_val)

        return 0

    @biophys_utils.ignore_warnings
    def get_allensdk_features(self, nwb_data_set, sweeps_by_type):
        #http://alleninstitute.github.io/AllenSDK/cell_types.html
        #http://alleninstitute.github.io/AllenSDK/_static/examples/nb/cell_types.html

        # From Gouwens, N. W., Berg, J., Feng, D., Sorensen, S. A., Zeng, H., Hawrylycz, M. J., … Arkhipov, A. (2018). Systematic generation of biophysically detailed models for diverse cortical neuron types. Nature Communications, 9(1). https://doi.org/10.1038/s41467-017-02718-3
        features_allensdk = {
            'avg_rate': [],  # Average firing frequency
            'peak_v': [],  # Action potential peak
            'adapt': [],  # Adaptation index (average difference between consecutive ISIs divided by their sum)
            'trough_v': [],
            # 'slow_trough_delta_v': [], #Slow trough depth #Needs to be computed
            # 'slow_trough_norm_t': [], #Time of slow trough (as fraction of ISI) #Needs to be computed
            'fast_trough_v': [],  # Fast trough depth
            'latency': [],  # seconds #Latency to first spike (spike=action potential)
            'mean_isi': [],  # seconds #Average interspike interval (ISI)
            'first_isi': [],  # seconds #Duration of first ISI
            'isi_cv': [],  # Coefficient of variation of ISIs
            'width': [],  # seconds #Action potential width at half-height (measured from trough to peak),
            # 'v_baseline': [], #Resting potential #This is ommitted in the paper (it's only in the supplementary material)
            'upstroke': [],
            'downstroke': [],
            'upstroke_v': [],
            'downstroke_v': [],
            'threshold_v': [],
        }

        # --------Extract features per sweep (with AllenSDK)--------------:
        # all_long_stimulus = sweeps_by_type['Square - 0.5ms Subthreshold'] + sweeps_by_type[
        #    'Square - 2s Suprathreshold'] + sweeps_by_type['Long Square']
        feature_values_allensdk = allen_extract_cell_features.extract_sweep_features(nwb_data_set, sweeps_by_type)
        # Mean of every sweep:
        for key, val in feature_values_allensdk.items():
            if len(val["spikes"]) > 1:  # Quality control
                #print("Sweep: ", key)
                val["spikes_features"] = {}
                for spike in val["spikes"]:
                    for key_spike, val_spike in spike.items():
                        if key_spike in val["spikes_features"]:
                            val["spikes_features"][key_spike].append(val_spike)
                        else:
                            val["spikes_features"][key_spike] = [val_spike]

                for key_allen, val_allen in features_allensdk.items():
                    if key_allen in val:
                        features_allensdk[key_allen].append(val[key_allen])
                    elif key_allen in val["spikes_features"]:
                        spike_feat = val["spikes_features"][key_allen]
                        features_allensdk[key_allen].append(np.mean(spike_feat))

        # Mean of all sweeps:
        for key, value in features_allensdk.items():
            if value is not None:
                features_allensdk[key] = np.mean(value)

        return features_allensdk

    def clean_biophys_simulation_sweep_files(self):
        biophys_models_dirs = next(os.walk(self.biophys_models_folder))[1]
        for model_name in biophys_models_dirs:
            model_dir = os.path.join(self.biophys_models_folder, model_name)
            model_responses_dir = os.path.join(model_dir, "model_sweeps_responses")
            for sweep_file in os.listdir(model_responses_dir):
                sweep_name = os.path.splitext(sweep_file)[0]
                sweep_extension = os.path.splitext(sweep_file)[1]
                if sweep_extension == ".txt" or sweep_extension == ".png": #Valid sweep file
                    sweep_file_path = os.path.join(model_responses_dir, sweep_file)
                    os.remove(sweep_file_path)

    def run_all_NEURON_simulations(self, grid_search=False, clean_old_sweeps=True):
        """
        Example: https://allensdk.readthedocs.io/en/latest/_static/examples/nb/pulse_stimulus.html

        NEURON download: https://neuron.yale.edu/neuron/getstd
        NEURON installation: install_neuron.sh
        """
        if clean_old_sweeps:
            self.clean_biophys_simulation_sweep_files()

        # configure NEURON -- this will infer model type (perisomatic vs. all-active)
        biophys_models_dirs = next(os.walk(self.biophys_models_folder))[1]
        #biophys_models_dirs = ['501349504']

        args = zip(biophys_models_dirs)#, range(len(biophys_models_dirs)))
        with mp.Pool() as pool:
            pool.starmap(self.run_NEURON_simulation, args)

        return 0

    def run_NEURON_simulation(self, model_name):
        print("-----------model_name: {}-------------".format(model_name))
        model_dir = os.path.join(self.biophys_models_folder, model_name)
        os.chdir(model_dir)

        compiled_modfiles_path = "x86_64"
        if not os.path.exists(compiled_modfiles_path):  # Compile modfiles (mechanisms)
            nrnivmodl_path = "/opt/conda/envs/conda_py364_tfm/x86_64/bin/nrnivmodl"
            cmd_compile = "{} modfiles".format(nrnivmodl_path)
            output = subprocess.Popen(cmd_compile, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output_out, output_err = output.communicate()
            if not os.path.exists(compiled_modfiles_path):
                raise Exception(str(output_err) + "; Set the proper nrnivmodl path")

        # Get specimen_id
        model_info_file = os.path.join(model_dir, 'model_info.json')
        with open(model_info_file, 'r') as file:
            model_info = json.load(file)
        specimen_id = model_info["specimen_id"]

        print("-----Simulation started for model: {} (specimen_id: {}) -----".format(model_name, specimen_id))

        # Get model configuration
        manifest_path = os.path.join(model_dir, "manifest.json")
        description = BiophysicalRunner.load_description(manifest_path)
        utils = BiophysicalUtils.create_utils(description)
        h = utils.h

        # Configure model
        morphology_path = description.manifest.get_path('MORPHOLOGY')
        utils.generate_morphology(morphology_path.encode('ascii', 'ignore'))
        utils.load_cell_parameters()

        junction_potential = description.data['fitting'][0]['junction_potential']

        print(h.topology())


        #stimulus_amplitudes = np.arange(0.17, 2.0, 0.4)
        #sections = ["soma", "axon", "dend", "apic"]
        #pos_sections = ["closest", "middle", "farthest"]

        stimulus_amplitudes = [0.17, 0.4, 0.5, 0.9, 1.5]
        sections = ["soma", "dend", "apic", "axon"]
        pos_sections = ["closest", "farthest"]

        for amp in stimulus_amplitudes:
            for iclamp_sect in sections:
                for iclamp_pos in pos_sections:
                    for record_sect in sections:
                        for record_pos in pos_sections:
                            try:
                                valid = AllenSDK.is_valid_stimulus(iclamp_sect, iclamp_pos, record_sect, record_pos)
                                if not valid:
                                    continue

                                stim = Stimulus(amp=amp, junction_potential=junction_potential)  # Default values

                                iclamp_sect_h = self.get_section_h(iclamp_sect, h)
                                iclamp_sect_pos_h = self.get_section_pos_h(iclamp_pos, iclamp_sect_h)
                                record_sect_h = self.get_section_h(record_sect, h)
                                record_sect_pos_h = self.get_section_pos_h(record_pos, record_sect_h)

                                iclamp_config = Iclamp_record_config(h, iclamp_sect_pos_h, record_sect_pos_h,
                                                                     iclamp_sect + "_" + iclamp_pos,
                                                                     record_sect + "_" + record_pos, stim,
                                                                     model_dir)

                                if not os.path.exists(iclamp_config.output_path):
                                    times, voltages = self.iclamp_and_record(h, iclamp_config, show_plot=False,
                                                                             save_txt=True, save_png=True)

                            except Exception as e:
                                print("This cell doesn't have apical dendrites")
        return 0

    @staticmethod
    def is_valid_stimulus(iclamp_sect, iclamp_pos, record_sect, record_pos):
        valid = True

        if (iclamp_sect == record_sect) and (iclamp_pos == record_pos):
            valid = False

        if iclamp_sect == "soma" and record_sect == "soma":
            valid = False

        if iclamp_sect == "soma" and iclamp_pos != "closest":
            valid = False

        if record_sect == "soma" and record_pos != "closest":
            valid = False

        if iclamp_sect == "axon" and iclamp_pos != "farthest":
            valid = False

        if record_sect == "axon" and record_pos != "farthest":
            valid = False

        return valid

    def get_section_pos_h(self, pos, sect_h):
        if pos == "closest":
            pos_h = sect_h[0]
        elif pos == "middle":
            pos_h = sect_h[int(len(sect_h) / 2)]
        elif pos == "farthest":
            pos_h = sect_h[-1]

        return pos_h

    def get_section_h(self, sect, h):
        if sect == "soma":
            sect_h = h.soma
        elif sect == "axon":
            sect_h = h.axon
        elif sect == "dend":
            sect_h = h.dend
        elif sect == "apic":
            sect_h = h.apic

        return sect_h

    def iclamp_and_record(self, h, iclamp_config, show_plot=False, save_txt=False, save_png=False):
        print("{} - Simulating iclamp and record: {} ...".format(iclamp_config.model_name, iclamp_config.name))
        # Setup iclamp
        h, stim_hoc = self.setup_iclamp(h, iclamp_config.section_iclamp, iclamp_config.stim)
        h.tstop = iclamp_config.stim.h_tstop

        # Setup record stimuli
        h, vec = self.record_stimuli(h, iclamp_config.section_record)

        # Run NEURON model
        h.finitialize()
        h.run()

        times = np.array(vec['t'])
        voltages = (np.array(vec['v']) - iclamp_config.stim.junction_potential)

        if show_plot or save_png:
            self.plot_response_stimulus(times, voltages, iclamp_config.full_name, iclamp_config.output_path, show_plot=show_plot, save_png=save_png)

        if save_txt:
            self.save_txt_response_stimulus(times, voltages, iclamp_config.output_path)

        return times, voltages

    def plot_response_stimulus(self, times, voltages, plot_title="", output_path="", show_plot=False, save_png=False):
        plt.plot(times, voltages)
        #plt.legend(plot, [plot_title])
        plt.title(plot_title)
        plt.xlabel('time (ms)')
        plt.ylabel('membrane potential (mV)')
        if save_png:
            plt.savefig("{}.png".format(output_path))
        if show_plot:
            plt.show()

        plt.clf()
        plt.cla()
        plt.close()

        return 0

    def save_txt_response_stimulus(self, times, voltages, output_path):

        output_data = np.transpose(np.vstack((times, voltages)))
        with open(output_path, "w") as f:
            np.savetxt(f, output_data)

        return 0

    def setup_iclamp(self, h, section_morpho=None, stim=None):
        if not section_morpho:
            section_morpho = h.soma[0]

        stim_hoc = h.IClamp(section_morpho(0.5))
        stim_hoc.delay = stim.delay
        stim_hoc.amp = stim.amp
        stim_hoc.dur = stim.dur

        return h, stim_hoc

    def record_stimuli(self, h, section_morpho=None):
        '''Set up output voltage recording.'''
        if not section_morpho:
            section_morpho = h.soma[0]

        vec = {"v": h.Vector(),
               "t": h.Vector()}

        vec["v"].record(section_morpho(0.5)._ref_v)
        vec["t"].record(h._ref_t)

        return h, vec

class Stimulus:
    def __init__(self, delay=50, amp=0.7, dur=200, junction_potential=-14.0):
        self.delay = delay
        self.amp = amp #Amplitude
        self.dur = dur #Duration
        self.h_tstop = dur + 100
        self.junction_potential = junction_potential

class Iclamp_record_config:
    def __init__(self, h, section_iclamp, section_record, origin_name="", destination_name="", stim=None, dir="", filename=""):
        if section_record is None:
            section_record = h.soma[0]
        if section_iclamp is None:
            section_iclamp = h.soma[0]

        self.origin_name = origin_name
        self.destination_name = destination_name
        self.name = origin_name + "_" + destination_name + "_amp_" + str(stim.amp)
        self.section_iclamp = section_iclamp
        self.section_record = section_record
        self.stim = stim
        self.dir = dir
        self.model_name = os.path.basename(dir)
        self.full_name = self.model_name + "_" + self.name

        output_dir = os.path.join(dir, "model_sweeps_responses")
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        if len(filename) == 0:
            self.filename = self.name
        self.output_path = os.path.join(output_dir, "simulation_output__{}.txt".format(self.filename))