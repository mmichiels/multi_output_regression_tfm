#!/usr/bin/env bash

CONDA_ENVS="/opt/conda/envs/"
CONDA_ENV="conda_py364_tfm"
CONDA_ENV_PATH=$CONDA_ENVS/$CONDA_ENV

function install_neuron() {
    cd /
    wget https://neuron.yale.edu/ftp/neuron/versions/v7.6/7.6.2/nrn-7.6.2.tar.gz
    tar xzf nrn-7.6.2.tar.gz
    rm nrn-7.6.2.tar.gz
    cd nrn-7.6
    #apt-get purge -y libx11-dev libxt-dev gnome-devel libncurses-dev xfonts-100dpi libbison-dev libfl-dev libc6-dev \
    #libc6 ncurses-term ncurses-bin ncurses-base libncursesw5-dev ncurses-dev \
    #libx11-dev libx11-data libice-dev libxcomposite-dev build-essential libreadline-dev libx11-dev libxext-dev libncurses-dev zlib1g-dev \
    #lib64ncurses5  lib32ncurses5 lib32ncurses5-dev lib32ncursesw5 lib32ncursesw5-dev lib32tinfo-dev lib32tinfo5 lib64ncurses5 \
    #lib64ncurses5-dev lib64tinfo5 libncurses5 libncurses5-dbg libncurses5-dev libncursesw5 libncursesw5-dbg libncursesw5-dev \
    #libtinfo-dev libtinfo5 libtinfo5-dbg libx32ncurses5 libx32ncurses5-dev libx32ncursesw5 libx32ncursesw5-dev libx32tinfo-dev \
    #libx32tinfo5 ncurses-base ncurses-bin ncurses-doc ncurses-examples ncurses-term
    #Other conda envs: conda uninstall gxx_linux-64
    # conda uninstall gcc_impl_linux-64
    # conda uninstall gcc_linux-64
    ./configure --prefix=`pwd` --without-iv --with-numpy --with-nrnpython=/opt/conda/envs/conda_py364_tfm/bin/python --prefix=/opt/conda/envs/conda_py364_tfm
    make; make install
    cd src/nrnpython
    python setup.py install
    ln -s $CONDA_ENV_PATH/x86_64/bin/nrniv $CONDA_ENV_PATH/bin/nrniv
    ln -s $CONDA_ENV_PATH/x86_64/bin/nrnivmodl $CONDA_ENV_PATH/bin/nrnivmodl
    export PATH="$PATH:$CONDA_ENV_PATH/x86_64/bin/"
}

install_neuron
