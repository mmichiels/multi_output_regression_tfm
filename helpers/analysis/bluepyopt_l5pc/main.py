from __future__ import print_function
from shutil import copyfile
import pandas as pd
import numpy as np
import os
from helpers import utils as biophys_utils
import bluepyopt.ephys as ephys
import bluepyopt
import pprint
import matplotlib.pyplot as plt
import neurom.viewer
import json
import l5pc_model
import l5pc_evaluator
from neuron import h, gui
from pyneuroml.neuron import export_to_neuroml2
from pylab import rcParams
rcParams['figure.figsize'] = 6.7, 5.27#4.7, 3.27

def plot_responses(responses, title=""):
    for index, (resp_name, response) in enumerate(sorted(responses.items())):
        plt.plot(response["time"], response["voltage"])
        # plt.legend(plot, [plot_title])
        plot_title = resp_name + " - ({})".format(title)
        plt.title(plot_title)
        plt.xlabel('time (ms)')
        plt.ylabel('membrane potential (mV)')
        #plt.show()
        dir = '../../../paper/images/electro'
        plt.savefig(os.path.join(dir, plot_title + ".png"))
        plt.close()
    """
    fig, axes = plt.subplots(len(responses), figsize=(10,10))
    for index, (resp_name, response) in enumerate(sorted(responses.items())):
        axes[index].plot(response['time'], response['voltage'], label=resp_name)
        axes[index].set_title(resp_name)
    fig.tight_layout()
    fig.show()
    """


def plot_scores(logs):
    gen_numbers = logs.select('gen')
    min_fitness = logs.select('min')
    max_fitness = logs.select('max')
    plt.plot(gen_numbers, min_fitness, label='min fitness')
    plt.xlabel('generation #')
    plt.ylabel('score (# std)')
    plt.legend()
    plt.xlim(min(gen_numbers) - 1, max(gen_numbers) + 1)
    plt.ylim(0.9 * min(min_fitness), 1.1 * max(min_fitness))
    #plt.show()
    dir = '../../../paper/images/electro'
    plot_title = "scores_bluepyopt"
    plt.savefig(os.path.join(dir, plot_title + ".png"))

def get_hoc_name_sectionlist(section_name):
    map_names = {
        "all": "all",
        "somatic": "soma",
        "axonal": "axon",
        "apical": "apic",
        "basal": "dend"
    }

    return map_names[section_name]

def create_fixed_params(all_params, fixed_params):
    all_params_fixed = {}

    all_params_fixed_sectionlist = set()
    for param, value in fixed_params.items():
        param_name = param.split('.')[0]
        sectionlist = param.split('.')[1]
        if sectionlist not in all_params_fixed:
            all_params_fixed[sectionlist] = {}
        all_params_fixed[sectionlist][param_name] =  value
        all_params_fixed_sectionlist.add(sectionlist)

    for param in all_params:
        if param["type"] != "section":
            continue

        name = param["param_name"]
        value = param["value"]
        sectionlist = param["sectionlist"]

        new_param = True
        if sectionlist in all_params_fixed_sectionlist:
            params_fixed_names = list(all_params_fixed[sectionlist])
            if name in params_fixed_names:
                new_param = False

        if new_param:
            if sectionlist not in all_params_fixed:
                all_params_fixed[sectionlist] = {}
            all_params_fixed[sectionlist][name] = value

    """
    all_params_fixed_path = os.path.join('output', 'parameters.json')
    with open(all_params_fixed_path, 'w') as params_file:
        json.dump(all_params_fixed, params_file)
    """

    return all_params_fixed

def create_mechs_params_json(model_dir_geppetto, id_model, mechanisms, all_params, fixed_params):
    parameters = create_fixed_params(all_params, fixed_params)

    mechs_params = {"mechanisms": {}, "parameters": {}}

    for sectionlist, channels in mechanisms.items():
        sectionlist_hoc_name = get_hoc_name_sectionlist(sectionlist)
        mechs_params["mechanisms"][sectionlist_hoc_name] = channels

    for sectionlist, params in parameters.items():
        sectionlist_hoc_name = get_hoc_name_sectionlist(sectionlist)
        mechs_params["parameters"][sectionlist_hoc_name] = params

    mechs_params_path = os.path.join(model_dir_geppetto, "config")
    mechs_params_path = os.path.join(mechs_params_path, 'mechanisms_and_parameters.json')
    with open(mechs_params_path, 'w') as params_file:
        json.dump(mechs_params, params_file)

    return mechs_params

def run_bluepyopt(electro_mu_df, electro_sigma_df, specimen_ids, specimen_files_folder, protocols_config, new_inference_data=False, show_morpho_plot=True):
    pp = pprint.PrettyPrinter(indent=2)

    for id in specimen_ids:
        #---Setup morphology---
        if new_inference_data:
            morpho_reconstruction = os.path.join(specimen_files_folder, str(id) + ".swc")
            #morpho_reconstruction = "morphology/C060114A7.asc"
        else:
            morpho_reconstruction = os.path.join(specimen_files_folder, str(id))
            morpho_reconstruction = os.path.join(morpho_reconstruction, "reconstruction.swc")

        neuron_morpho = neurom.load_neuron(morpho_reconstruction)
        num_apical_dends = neurom.get('segment_lengths', neuron_morpho, neurite_type=neurom.APICAL_DENDRITE)
        if len(num_apical_dends) == 0:
            print("This specimen ({}) doesn't have apical dendrites".format(id))
            continue

        if show_morpho_plot:
            neurom.viewer.draw(neuron_morpho)
            plt.title(str(id))
            #plt.show()
            dir = '../../../paper/images/morpho'
            plt.savefig(os.path.join(dir, str(id) + '.png'))
            plt.close()

        morphology = ephys.morphologies.NrnFileMorphology(morpho_reconstruction, do_replace_axon=True)

        #---Setup parameters and mechanisms---
        param_configs = json.load(open('config/parameters.json'))
        #print([param_config['param_name'] for param_config in param_configs])

        parameters = l5pc_model.define_parameters()
        #print('\n'.join('%s' % param for param in parameters))

        mechanisms_configs = json.load(open('config/mechanisms.json'))
        mechanisms = l5pc_model.define_mechanisms()
        #print('\n'.join('%s' % mech for mech in mechanisms))

        #---Setup cell model---
        l5pc_cell = ephys.models.CellModel('l5pc', morph=morphology, mechs=mechanisms, params=parameters)
        #print(l5pc_cell)

        param_names = [param.name for param in l5pc_cell.params.values() if not param.frozen]

        #---Setup protocols---
        #print(protocols_config)

        fitness_protocols = l5pc_evaluator.define_protocols(protocols_config)#, neuron_morpho)
        #print('\n'.join('%s' % protocol for protocol in fitness_protocols.values()))

        #---Setup features and evaluator---
        features_configs = {}
        features_specimen_mu = electro_mu_df[electro_mu_df["specimen_id"] == id]#.to_dict(orient='records')[0]
        features_specimen_std = electro_sigma_df[electro_mu_df["specimen_id"] == id]#.to_dict(orient='records')[0]
        for proto_config in protocols_config:
            features_proto = [col for col in electro_mu_df.columns if col.startswith(proto_config["name"])]
            features_specimen_proto_mu = features_specimen_mu.loc[:, features_proto].to_dict(orient='records')[0]
            features_specimen_proto_std = features_specimen_std.loc[:, features_proto].to_dict(orient='records')[0]
            feats_specimen_proto = {}
            for feat, mu in features_specimen_proto_mu.items():
                feat_name = feat.split('__')[1]
                std = features_specimen_proto_std[feat]
                feats_specimen_proto[feat_name] = [mu, abs(std)] #mean , std #0.05*abs(mu)

                features_configs[proto_config["name"]] =  {
                    proto_config["record_sect"]: feats_specimen_proto
                }

        fitness_calculator = l5pc_evaluator.define_fitness_calculator(fitness_protocols, features_configs)
        #print(fitness_calculator)

        sim = ephys.simulators.NrnSimulator()

        evaluator = ephys.evaluators.CellEvaluator(
            cell_model=l5pc_cell,
            param_names=param_names,
            fitness_protocols=fitness_protocols,
            fitness_calculator=fitness_calculator,
            sim=sim)

        #---Run original protocols---
        release_params = {
            'gNaTs2_tbar_NaTs2_t.apical': 0.026145,
            'gSKv3_1bar_SKv3_1.apical': 0.004226,
            'gImbar_Im.apical': 0.000143,
            'gNaTa_tbar_NaTa_t.axonal': 3.137968,
            'gK_Tstbar_K_Tst.axonal': 0.089259,
            'gamma_CaDynamics_E2.axonal': 0.002910,
            'gNap_Et2bar_Nap_Et2.axonal': 0.006827,
            'gSK_E2bar_SK_E2.axonal': 0.007104,
            'gCa_HVAbar_Ca_HVA.axonal': 0.000990,
            'gK_Pstbar_K_Pst.axonal': 0.973538,
            'gSKv3_1bar_SKv3_1.axonal': 1.021945,
            'decay_CaDynamics_E2.axonal': 287.198731,
            'gCa_LVAstbar_Ca_LVAst.axonal': 0.008752,
            'gamma_CaDynamics_E2.somatic': 0.000609,
            'gSKv3_1bar_SKv3_1.somatic': 0.303472,
            'gSK_E2bar_SK_E2.somatic': 0.008407,
            'gCa_HVAbar_Ca_HVA.somatic': 0.000994,
            'gNaTs2_tbar_NaTs2_t.somatic': 0.983955,
            'decay_CaDynamics_E2.somatic': 210.485284,
            'gCa_LVAstbar_Ca_LVAst.somatic': 0.000333
        }


        """
        hoc_string = l5pc_cell.create_hoc(release_params)
        hoc_file_path = os.path.join(output_path, '{}.hoc'.format(id))
        with open(hoc_file_path, 'w') as output_hoc:
            output_hoc.write(hoc_string)
        """
        print("---Running original protocols---")
        release_responses = evaluator.run_protocols(protocols=fitness_protocols.values(), param_values=release_params)
        plot_responses(release_responses, title="Original - " + str(id))
        objectives = fitness_calculator.calculate_scores(release_responses)
        print("=========================================================")
        print("RANDOM PARAMS objectives:", objectives)
        print("=========================================================")

        #---DEAPOptimisation---
        print("---Running DEAPOptimisation---")
        opt = bluepyopt.optimisations.DEAPOptimisation(evaluator=evaluator, offspring_size=1)
        final_pop, halloffame, logs, hist = opt.run(max_ngen=1, cp_filename='checkpoints/checkpoint.pkl')
        print("DEAPOptimisation done")

        #print(halloffame[0])
        best_params = evaluator.param_dict(halloffame[0])
        #print(pp.pprint(best_params))

        #--Run protocols for the best model---
        print("---Running protocols for the best model---")
        best_responses = evaluator.run_protocols(protocols=fitness_protocols.values(), param_values=best_params)
        objectives = halloffame[0].fitness.values
        print("=========================================================")
        print("FITTED PARAMS objectives:", objectives)
        print("=========================================================")
        plot_responses(best_responses, title="Model - " + str(id))
        plot_scores(logs)

        convert_model_to_geppetto(id, mechanisms_configs, morpho_reconstruction, param_configs, best_params)


def convert_model_to_geppetto(id, mechanisms_configs, morpho_reconstruction, param_configs, release_params):
    # Setup morpho reconstruction file:
    project_dir = os.path.abspath('../../../')
    model_dir_geppetto = os.path.join(project_dir, "neuron_ui")
    model_dir_geppetto = os.path.join(model_dir_geppetto, "NEURON-UI")
    model_dir_geppetto = os.path.join(model_dir_geppetto, "neuron_ui")
    model_dir_geppetto = os.path.join(model_dir_geppetto, "models")
    morpho_dir_geppetto = os.path.join(model_dir_geppetto, "reconstruction.swc")
    copyfile(morpho_reconstruction, morpho_dir_geppetto)

    # Setup mechs and params file:
    create_mechs_params_json(model_dir_geppetto, id, mechanisms_configs, all_params=param_configs, fixed_params=release_params)

    return 0


if __name__ == "__main__":
    # IMPORTANT: Run this command in the cmd at the beggining: nrnivmodl mechanisms

    electro_mu_df = pd.read_csv("../../machine_learning/neural_networks/output_data/targets_predicted_mu.csv", na_filter=False, low_memory=False)
    electro_sigma_df = pd.read_csv("../../machine_learning/neural_networks/output_data/targets_predicted_sigma.csv", na_filter=False, low_memory=False)
    protocols_config_df = pd.read_csv("../../machine_learning/neural_networks/output_data/protocols_config.csv", na_filter=False, low_memory=False)

    protocols_config = protocols_config_df.to_dict(orient='records')
    specimen_ids = electro_mu_df.loc[:, "specimen_id"].values

    new_inference_data = False
    if new_inference_data:
        specimen_ids = specimen_ids.astype(np.object)
        specimen_files_folder = "../../input_data/new_morpho_reconstructions/"
    else:
        specimen_ids = specimen_ids.astype(np.int)
        specimen_files_folder = "../../input_data/specimen_files/"

    run_bluepyopt(electro_mu_df, electro_sigma_df, specimen_ids, specimen_files_folder, protocols_config, new_inference_data)

    done = True
