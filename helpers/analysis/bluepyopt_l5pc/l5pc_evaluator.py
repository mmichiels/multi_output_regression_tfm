"""Run simple cell optimisation"""
import neurom

"""
Copyright (c) 2016, EPFL/Blue Brain Project

 This file is part of BluePyOpt <https://github.com/BlueBrain/BluePyOpt>

 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License version 3.0 as published
 by the Free Software Foundation.

 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 details.

 You should have received a copy of the GNU Lesser General Public License
 along with this library; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=R0914

import os
import json

import l5pc_model  # NOQA

import bluepyopt.ephys as ephys

script_dir = os.path.dirname(__file__)
config_dir = os.path.join(script_dir, 'config')

# TODO store definition dicts in json
# TODO rename 'score' into 'objective'
# TODO add functionality to read settings of every object from config format


def define_protocols(protocols_config):
    """Define protocols"""
    protocols = {}

    for protocol_config in protocols_config:
        if protocol_config["iclamp_pos"] == "closest":
            iclamp_sec_index = 0
        else:
            iclamp_sec_index = -1
        iclamp_loc = ephys.locations.NrnSeclistCompLocation(
            name=protocol_config["iclamp_sect"],
            seclist_name=protocol_config["iclamp_sect"], #'somatic',
            sec_index=iclamp_sec_index,
            comp_x=0.5)

        stimuli = [
            ephys.stimuli.NrnSquarePulse(
                step_amplitude=protocol_config['amp'],
                step_delay=protocol_config['delay'],
                step_duration=protocol_config['duration'],
                location=iclamp_loc,
                total_duration=protocol_config['duration'])
        ]

        if protocol_config["record_pos"] == "closest":
            record_sec_index = 0
        else:
            record_sec_index = -1


        record_loc = ephys.locations.NrnSeclistCompLocation(
            name=protocol_config["record_sect"],
            seclist_name=protocol_config["record_sect"],#'somatic',
            sec_index=record_sec_index,
            comp_x=0.5)

        recordings = [ephys.recordings.CompRecording(
            name='%s.v' %protocol_config["name"],
            location=record_loc,
            variable='v')
        ]

        protocols[protocol_config["name"]] = ephys.protocols.SweepProtocol(
            protocol_config["name"],
            stimuli,
            recordings)

    return protocols


def define_fitness_calculator(protocols, feature_definitions):
    """Define fitness calculator"""

    # TODO: add bAP stimulus
    objectives = []

    for protocol_name, locations in feature_definitions.items():
        for location, features in locations.items():
            for efel_feature_name, meanstd in features.items():
                if location == 'soma':
                    threshold = -20
                else:
                    threshold = -55

                feature_name = '%s.%s.%s' % (
                    protocol_name, location, efel_feature_name)
                recording_names = {'': '%s.v' % (protocol_name)}
                stimulus = protocols[protocol_name].stimuli[0]

                stim_start = stimulus.step_delay

                stim_end = stim_start + stimulus.step_duration

                feature = ephys.efeatures.eFELFeature(
                    feature_name,
                    efel_feature_name=efel_feature_name,
                    recording_names=recording_names,
                    stim_start=stim_start,
                    stim_end=stim_end,
                    exp_mean=meanstd[0],
                    exp_std=meanstd[1],
                    threshold=threshold
                )
                objective = ephys.objectives.SingletonObjective(
                    feature_name,
                    feature)
                objectives.append(objective)

    fitcalc = ephys.objectivescalculators.ObjectivesCalculator(objectives)

    return fitcalc


def create():
    """Setup"""

    l5pc_cell = l5pc_model.create()

    fitness_protocols = define_protocols()
    fitness_calculator = define_fitness_calculator(fitness_protocols)

    param_names = [param.name
                   for param in l5pc_cell.params.values()
                   if not param.frozen]

    sim = ephys.simulators.NrnSimulator()

    return ephys.evaluators.CellEvaluator(
        cell_model=l5pc_cell,
        param_names=param_names,
        fitness_protocols=fitness_protocols,
        fitness_calculator=fitness_calculator,
        sim=sim)
