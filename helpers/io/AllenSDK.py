from .base import  *

import json
from collections import defaultdict
from allensdk.core.cell_types_cache import CellTypesCache
from allensdk.api.queries.cell_types_api import CellTypesApi
from allensdk.core.cell_types_cache import ReporterStatus as RS
from allensdk.api.queries.biophysical_api import BiophysicalApi
from allensdk.core.nwb_data_set import NwbDataSet
import allensdk.model.biophysical.runner as BiophysicalRunner
import allensdk.model.biophysical.utils as BiophysicalUtils
from allensdk.ephys.extract_cell_features import extract_cell_features
from allensdk.ephys.ephys_extractor import EphysSweepSetFeatureExtractor, _step_stim_amp
import allensdk.ephys.extract_cell_features as allen_extract_cell_features
import efel
from matplotlib import pyplot as plt
from itertools import repeat
import multiprocessing as mp
from helpers import analysis as biophys_analysis

class AllenSDK(Io):
    def __init__(self, session_key=1, empty=False):
        super(AllenSDK, self).__init__()

        self.input_data_folder = os.path.dirname(os.path.abspath(__file__)) + "/../input_data"
        self.specimen_files_folder = os.path.join(self.input_data_folder, "specimen_files")
        self.biophys_models_folder = os.path.join(self.input_data_folder, "biophysical_models")

        self.allensdk_analysis = biophys_analysis.AllenSDK(self.input_data_folder, self.specimen_files_folder,
                                                           self.biophys_models_folder)

        if not empty:
            self.cell_types_cache_path = os.path.join(self.input_data_folder, str(session_key))
            manifest_file = os.path.join(self.cell_types_cache_path, "manifest.json")
            self.cell_types_cache = CellTypesCache(manifest_file=manifest_file)
            self.cell_types_api = CellTypesApi()
            self.cells_metadata_with_reconstructions = self.cell_types_cache.get_cells(require_reconstruction=True)
            self.specimen_ids_morpho_electro = list(set([cell["id"] for cell in self.cells_metadata_with_reconstructions])) #Set to remove duplicates

    #-------Morpho, electro-----------------------
    def get_morpho_electro_metadata_dfs(self):
        morpho_df = self.get_morpho_dataframe()
        morpho_ids = morpho_df["specimen_id"].values.tolist()

        electro_df = self.get_electro_dataframe()
        electro_df = electro_df.loc[electro_df['specimen_id'].isin(morpho_ids)]
        electro_df.reset_index(drop=True, inplace=True)

        metadata_df = self.get_metadata_dataframe()
        metadata_df = metadata_df.loc[metadata_df['id'].isin(morpho_ids)]
        metadata_df.reset_index(drop=True, inplace=True)

        morpho_df.to_csv(os.path.join(self.input_data_folder, "raw_morpho.csv"), index=False)
        electro_df.to_csv(os.path.join(self.input_data_folder, "raw_electro_website.csv"), index=False)
        metadata_df.to_csv(os.path.join(self.input_data_folder, "raw_metadata.csv"), index=False)

        return morpho_df, electro_df, metadata_df

    def get_morpho_dataframe(self, query_filters={}):
        morpho_features = self.cell_types_cache.get_morphology_features()
        morpho_df = pd.DataFrame(morpho_features)

        morpho_df = self.clean_features_dataframe(morpho_df, column_id="specimen_id")

        return morpho_df

    def get_electro_dataframe(self, query_filters={}):
        electro_features = self.cell_types_cache.get_ephys_features()
        electro_df = pd.DataFrame(electro_features)

        electro_df = self.clean_features_dataframe(electro_df, column_id="specimen_id")

        return electro_df

    def get_metadata_dataframe(self):
        metadata_df = pd.DataFrame(self.cells_metadata_with_reconstructions)

        return metadata_df

    def get_morpho_electro_files(self):
        #self.get_morpho_files()
        self.get_electro_files()

        return 0

    def get_morpho_files(self, verbose=True):
        if not os.path.exists(self.specimen_files_folder):
            os.makedirs(self.specimen_files_folder)

        for i, specimen_id in enumerate(self.specimen_ids_morpho_electro):
            if verbose:
                print("Downloading morpho files of specimen {} of {} ...".format(i, len(self.specimen_ids_morpho_electro)))

            specimen_dir = os.path.join(self.specimen_files_folder, str(specimen_id))
            if not os.path.exists(specimen_dir):
                os.makedirs(specimen_dir)

            #Download SWC file:
            path_swc = os.path.join(specimen_dir, "reconstruction.swc")
            self.cell_types_api.save_reconstruction(specimen_id, path_swc)

        return 0

    def get_electro_files(self, verbose=True):
        if not os.path.exists(self.specimen_files_folder):
            os.makedirs(self.specimen_files_folder)

        #for i, specimen_id in enumerate(self.specimen_ids_morpho_electro):

        args = zip(self.specimen_ids_morpho_electro, range(len(self.specimen_ids_morpho_electro)), repeat(verbose))
        with mp.Pool() as pool:
            pool.starmap(self.get_electro_file, args)

        return 0

    def get_electro_file(self, specimen_id ,i, verbose):
        print("Process: ", mp.current_process().name)
        specimen_dir = os.path.join(self.specimen_files_folder, str(specimen_id))
        if not os.path.exists(specimen_dir):
            os.makedirs(specimen_dir)

        # Download NWB file:
        path_nwb = os.path.join(specimen_dir, "ephys_data.nwb")
        if not os.path.exists(path_nwb):
            if verbose:
                print("Downloading electro files of specimen {} of {} ...".format(i, len(self.specimen_ids_morpho_electro)))
            self.cell_types_api.save_ephys_data(specimen_id, path_nwb)

        # Download epyhs_sweeps.json:
        path_ephys_sweeps = os.path.join(specimen_dir, 'ephys_sweeps.json')
        if not os.path.exists(path_ephys_sweeps):
            self.download_ephys_sweeps_json(specimen_id, path_ephys_sweeps)

        return 0

    def clean_features_dataframe(self, dataframe, column_id):

        dataframe = dataframe.loc[:, ~dataframe.columns.duplicated()]
        dataframe.dropna()
        dataframe = dataframe.loc[dataframe[column_id].isin(self.specimen_ids_morpho_electro)]
        dataframe.drop_duplicates(keep=False, inplace=True)
        dataframe.drop_duplicates(subset=column_id, keep=False, inplace=True)
        dataframe.sort_values([column_id], axis=0, inplace=True)
        dataframe.reset_index(drop=True, inplace=True)

        return dataframe

    def download_ephys_sweeps_json(self, id, path):
        ephys_sweeps = self.cell_types_cache.get_ephys_sweeps(id)
        if ephys_sweeps != []:
            with open(path, 'w') as outfile:
                json.dump(ephys_sweeps, outfile)

        return 0

    def get_biophys_models(self):
        bp = BiophysicalApi()
        bp.cache_stimulus = True  # change to False to not download the large stimulus NWB file
        biophys_models_info = bp.get_neuronal_models(specimen_ids=self.specimen_ids_morpho_electro)#, model_type_ids=["all-active"])

        #The manifest.json downloaded with the API is wrong (the sweeps are not grouped by type).
        #The proper manifest.json files can be found in the Allen website
        for model_info in biophys_models_info:
            dir = os.path.join(self.biophys_models_folder, str(model_info["id"]))
            #bp.cache_data(model_id, working_directory=dir) #Download model files (but no ephys_sweeps.json)

            #Download ephys_sweeps.json:
            ephys_sweeps_path = os.path.join(dir, 'ephys_sweeps.json')
            self.download_ephys_sweeps_json(model_info["specimen_id"], ephys_sweeps_path)
            model_info_path = os.path.join(dir, 'model_info.json')
            with open(model_info_path, 'w') as outfile:
                json.dump(model_info, outfile)


        return self.biophys_models_folder

    @staticmethod
    def get_fields_by_categories():
        ALLEN_CELL_TYPES_CATEGORIES_FRONTEND = [
            {
                "name": "animal",
                "verbose_name": "Animal",
                "fields":
                    [
                        {
                            "name": "species",
                            "verbose_name": "Species"
                        },
                    ]
            },
            {
                "name": "Reconstruction type",
                "verbose_name": "Completeness",
                "fields":
                    [
                        {
                            "name": "domain",
                            "verbose_name": "Structural domain"
                        }
                    ]
            },
            {
                "name": "morphometry",
                "verbose_name": "Morphometry",
                "fields":
                    [
                        {
                            "name": "surface",
                            "verbose_name": "Surface"
                        },
                        {
                            "name": "shrinkage_corrected",
                            "verbose_name": "Shrinkage corrected"
                        },
                    ]
            },
            {
                "name": "other_fields",
                "verbose_name": "Other fields",
                "fields":
                    [

                    ]
            },
            {
                "name": "custom_options",
                "verbose_name": "Custom options",
                "fields":
                    [
                        {
                            "name": "limit_number_results",
                            "verbose_name": "Limit the number of results",
                            "custom_option": True
                        },
                        {
                            "name": "sort_results",
                            "verbose_name": "Sort results by",
                            "custom_option": True
                        },
                    ]
            },
        ]

        return ALLEN_CELL_TYPES_CATEGORIES_FRONTEND


    @staticmethod
    def read_stimulus_txt(input_path, show_plots=False):
        with open(input_path, 'r') as input_file:
            times, voltages = np.loadtxt(input_file, delimiter=' ', unpack=True)

        if show_plots:
            plt.plot(times, voltages)
            plt.xlabel('time (ms)')
            plt.ylabel('membrane potential (mV)')
            plt.show()

        return times, voltages

    def get_electro_features(self):
        self.allensdk_analysis.get_electro_features(verbose=True)

        return 0

    def get_biophys_electro_features(self):
        self.allensdk_analysis.get_biophys_electro_features(verbose=True)

        return 0

    def get_all_biophys_models_electro_features(self):
        self.allensdk_analysis.get_all_biophys_models_electro_features()

        return 0

    def get_biophys_simulation_sweeps(self):
        self.allensdk_analysis.run_all_NEURON_simulations()

        return 0

def target_features(ext):
    """Determine target features from sweeps in set extractor"""
    ext.process_spikes()
    for swp in ext.sweeps():
        swp.sweep_feature("v_baseline") # Pre-compute all the baselines, too
        swp.process_new_spike_feature("slow_trough_norm_t",
                                      slow_trough_norm_t,
                                      affected_by_clipping=True)
        swp.process_new_spike_feature("slow_trough_delta_v",
                                      slow_trough_delta_voltage_feature,
                                      affected_by_clipping=True)


    sweep_keys = swp.sweep_feature_keys()
    spike_keys = swp.spike_feature_keys()

    min_std_dict = {
        'avg_rate': 0.5,
        'adapt': 0.001,
        'peak_v': 2.0,
        'trough_v': 2.0,
        'fast_trough_v': 2.0,
        'slow_trough_delta_v': 0.5,
        'slow_trough_norm_t': 0.05,
        'latency': 5.0 * 1e-3, # seconds
        'isi_cv': 0.1,
        'mean_isi': 0.5 * 1e-3, # seconds
        'first_isi': 1.0 * 1e-3, # seconds
        'v_baseline': 2.0,
        'width': 0.1 * 1e-3, # seconds
        'upstroke': 5.0,
        'downstroke': 5.0,
        'upstroke_v': 2.0,
        'downstroke_v': 2.0,
        'threshold_v': 2.0,
    }

    target_features = []
    for k in min_std_dict:
        if k in sweep_keys:
            values = ext.sweep_features(k)
        elif k in spike_keys:
            values = ext.spike_feature_averages(k)
        else:
            print("Could not find feature: ", k)
            continue

        t = {"name": k, "mean": float(values.mean()), "stdev": float(values.std())}
        if min_std_dict[k] > t["stdev"]:
            t["stdev"] = min_std_dict[k]
        target_features.append(t)

    return target_features

def get_sweep_v_i_t_from_set(data_set, sweep_number):
    sweep_data = data_set.get_sweep(sweep_number)
    i = sweep_data["stimulus"] # in A
    v = sweep_data["response"] # in V
    i *= 1e12 # to pA
    v *= 1e3 # to mV
    sampling_rate = sweep_data["sampling_rate"] # in Hz
    t = np.arange(0, len(v)) * (1.0 / sampling_rate)
    return v, i, t

def slow_trough_norm_t(swp):
    """
    threshold_t = swp.spike_feature("threshold_t")
    slow_trough_t = swp.spike_feature("slow_trough_t", include_clipped=True)
    trough_t = swp.spike_feature("trough_t", include_clipped=True)
    """
    threshold_t = swp.spike_feature("threshold_t")
    slow_trough_t = swp.spike_feature("slow_trough_t", include_clipped=True)
    trough_t = swp.spike_feature("trough_t", include_clipped=True)

    # if slow trough is undefined, use overall trough value instead
    nan_mask = np.isnan(slow_trough_t)
    slow_trough_t[nan_mask] = trough_t[nan_mask]

    if len(threshold_t) == 0:
        return np.array([])
    elif len(threshold_t) == 1:
        return np.array([(slow_trough_t[0] - threshold_t[0]) / (swp.end - threshold_t[0])])

    isis = np.diff(threshold_t)
    isis = np.append(isis, (swp.end - threshold_t[-1]))
    trough_intervals = slow_trough_t - threshold_t
    return trough_intervals / isis


def slow_trough_delta_voltage_feature(swp):
    return spike_feature_difference(swp, "fast_trough_v", "slow_trough_v")


def spike_feature_difference(swp, key_1, key_2, zero_nans=True):
    delta_values = (swp.spike_feature(key_1, include_clipped=True) -
                    swp.spike_feature(key_2, include_clipped=True))
    if zero_nans:
        delta_values[np.isnan(delta_values)] = 0.
    return delta_values

