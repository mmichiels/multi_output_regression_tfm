from helpers import utils
import numpy as np
import pandas as pd
import os
from datetime import datetime
import plotly
import plotly.graph_objs as plotly_graph
import plotly.figure_factory as plotly_figures
import plotly.tools as plotly_tools
import scipy.stats as scipy_stats

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils import data
#import multprocessing
import mkl
import numba
from tensorboardX import SummaryWriter
#from utils import profilers
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import KFold
from sklearn.metrics import mutual_info_score
from operator import itemgetter
from profilehooks import profile
from profilehooks import timecall

from helpers.analysis import AllenSDK

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
#print("NUMPY CONFIG: ")
#np.show_config()

class DatasetANN(data.Dataset):
    def __init__(self, inputs, targets, cuda=True, device_cuda=""):
        'Initialization'
        if cuda: #Preallocate all dataset in the GPU (this creates the tensors in the GPU, not creates them in the CPU and transfer the to the GPU)
            print("Preallocating tensors in the GPU...")
            self.inputs = torch.tensor(inputs.values, dtype=torch.float32, device="cuda:0")
            self.targets = torch.tensor(targets.values, dtype=torch.float32, device="cuda:0")
        else:
            self.inputs = torch.tensor(inputs.values, dtype=torch.float32)
            self.targets = torch.tensor(targets.values, dtype=torch.float32)

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.inputs)

    def __getitem__(self, index):
        'Generates one sample of data'
        X = self.inputs[index, :]
        y = self.targets[index, :]

        return X, y


class NeuralNetwork(nn.Module):

    def __init__(self, input_dim, output_dim, num_hidden_layers=3, neurons_per_hlayer=1500, dropout=False, dropout_rate=0.4,
                 reg_l1=False, lr=0.01, k_folds=4, n_gaussians=3, cuda=False):
        super(NeuralNetwork, self).__init__()

        self.num_hidden_layers = num_hidden_layers
        self.neurons_per_hlayer = neurons_per_hlayer
        self.n_gaussians = n_gaussians

        self.input = nn.Linear(input_dim, self.neurons_per_hlayer)  #fc = fully connected
        self.fc_hidden = nn.Linear(self.neurons_per_hlayer, self.neurons_per_hlayer)
        self.fc_output_pi = nn.Linear(self.neurons_per_hlayer, self.n_gaussians*output_dim)
        self.fc_output_mu = nn.Linear(self.neurons_per_hlayer, self.n_gaussians*output_dim)
        self.fc_output_sigma = nn.Linear(self.neurons_per_hlayer, self.n_gaussians*output_dim)

        self.dropout = dropout
        if self.dropout:
            self.dropout_rate = dropout_rate
            self.dropout_layer = nn.Dropout(self.dropout_rate)  #fc = fully connected
        self.reg_l1 = False
        self.lr = lr
        self.k_folds = k_folds

        self.name = '{}__{}_dropout_{}_{}_reg_l1_{}_lr_{}'.format(num_hidden_layers, neurons_per_hlayer, dropout, dropout_rate, reg_l1, lr)

        if cuda:
            if torch.cuda.is_available():
                self.in_cuda = True
            else:
                self.in_cuda = False
                print("CUDA is not available in this device")
        else:
            self.in_cuda = False

        if self.in_cuda:
            torch.backends.cudnn.benchmark = True
            print("Running model in CUDA: ", torch.cuda.get_device_name(0))

            self.cuda("cuda:0")
        else:
            print("Running model in CPU")
            print("MKL: ", torch.backends.mkl.is_available())
            torch.set_num_threads(int(os.cpu_count() / 2)) #This sets the OpenMP threads and also the mkl threads
            #mkl.set_num_threads(1)
            print("OPENMP_NUM_THREADS: ", torch.get_num_threads())
            print("MKL_NUM_THREADS: ", mkl.get_max_threads())

    def forward(self, x):
        x = self.input(x)
        x = F.relu(x)
        if self.dropout:
            x = self.dropout_layer(x)

        for i in range(self.num_hidden_layers):
            x = self.fc_hidden(x)
            x = F.relu(x)
            if self.dropout:
                x = self.dropout_layer(x)

        pi = F.softmax(self.fc_output_pi(x), -1)
        mu = self.fc_output_mu(x)
        sigma = torch.exp(self.fc_output_sigma(x))

        return pi, mu, sigma

def reinitialize_params(m):
    if isinstance(m, nn.Linear):
        nn.init.xavier_uniform(m.weight.data)
        nn.init.zeros_(m.bias)

def normalize_dataframe(array):
    scaler = StandardScaler()
    scaler = scaler.fit(array)
    scaled_array = scaler.transform(array)

    scaled_tensor = torch.tensor(scaled_array, dtype=torch.float32)

    return scaled_tensor, scaler

def nll_loss(y, pi, mu, sigma):
    gaussian_dist = torch.distributions.Normal(loc=mu, scale=sigma)
    log_lik = gaussian_dist.log_prob(y)

    loss = torch.sum(log_lik * pi, dim=1)
    loss = torch.log(-torch.mean(loss))

    return torch.mean(loss)

def training(input_df, target_df, cuda=True):
    # Shuffle dataset
    input_df = input_df.sample(frac=1, random_state=1) #random_state is the seed
    target_df = target_df.reindex(input_df.index)
    input_df = input_df.reset_index(drop=True)
    target_df = target_df.reset_index(drop=True)
    # Training-set splits
    training_val_size = int(input_df.shape[0] * 0.8) #For holdout validation
    specimen_ids_test = input_df.iloc[training_val_size:, :].loc[:, ["specimen_id"]].values
    input_df = input_df.drop(columns=["specimen_id"])
    train_val_set = DatasetANN(input_df.iloc[0:training_val_size, :], target_df.iloc[0:training_val_size, :], cuda)
    test_set = DatasetANN(input_df.iloc[training_val_size:, :], target_df.iloc[training_val_size:, :], cuda)

    combinations_num_hlayers = [3]#np.arange(2, 4)
    combinations_neurons_per_hlayer = [616]#np.arange(input_df.shape[1], 1050, 300)
    combinations_dropout = [True]#[False, True]
    combinations_dropout_rate = [0.6]#[0.4, 0.6]#, 0.6]
    combinations_regularization_l1 = [False]#[False, True]
    combinations_learning_rate = [0.001]#[0.01, 0.001]
    k_folds = 4
    best_loss_val = float("inf")
    best_num_epochs = 0
    best_model_val = None
    for num_hidden_layers in combinations_num_hlayers:
        for neurons_per_hlayer in combinations_neurons_per_hlayer:
            for do_dropout in combinations_dropout:
                if do_dropout:
                    do_combinations_dropout_rate = combinations_dropout_rate
                else:
                    do_combinations_dropout_rate = [0]
                for dropout_rate in do_combinations_dropout_rate:
                    for l1_reg in combinations_regularization_l1:
                        for lr in combinations_learning_rate:
                            model = NeuralNetwork(input_df.shape[1], target_df.shape[1], num_hidden_layers,
                                                  neurons_per_hlayer, do_dropout, dropout_rate, l1_reg,
                                                  lr, k_folds, cuda=cuda)
                            print("Model: ", model)

                            criterion = {}
                            mse_criterion = nn.MSELoss()
                            #optimizer = optim.SGD(model.parameters(), lr=lr, momentum=0.9, weight_decay=0.001)
                            optimizer = optim.Adam(model.parameters(), lr=lr)#, momentum=0.09) #lr=0.003)
                            #optimizer = optim.LBFGS(model.parameters(), lr=0.1)#, weight_decay=1)

                            writer_logs = create_tensorboard_logs(input_df, model)

                            model_loss_val, model_num_epochs = training_loop(criterion, model, optimizer, train_val_set, mse_criterion, writer_logs)
                            if model_loss_val < best_loss_val:
                                best_loss_val = model_loss_val
                                best_num_epochs = model_num_epochs
                                best_model_val = model

    best_model = train_whole_best_model(best_model_val, train_val_set, best_num_epochs, criterion, mse_criterion, writer_logs, cuda)
    best_model_path = 'saved_models/{}.pt'.format(best_model.name)
    torch.save(best_model, best_model_path)
    print("=====================================================")
    print('Finished Training')
    print("---Best model---")
    print("Loss cross validation: {}".format(best_loss_val))
    print("Hidden layers: {} Neurons per hlayer: {}; Dropout: {} {}; Reg L1: {}; Lr: {}; Num epochs: {}".format(best_model.num_hidden_layers,
                                                                                                                best_model.neurons_per_hlayer, best_model.dropout, best_model.dropout_rate, best_model.reg_l1, best_model.lr, best_num_epochs))
    print("=====================================================")

    #Try untrained model to then compare results with the trained network
    test_random_model(test_set, best_model.num_hidden_layers, best_model.neurons_per_hlayer, cuda)

    return best_model_path, train_val_set, test_set, specimen_ids_test


def create_tensorboard_logs(input_df, model):
    date_now = datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    writer_logs = SummaryWriter('logs/{}_cross_validation'.format(model.name))
    if model.in_cuda:
        dummy_log_input = torch.tensor(input_df.iloc[0, :].values, dtype=torch.float32, device="cuda:0")
    else:
        dummy_log_input = torch.tensor(input_df.iloc[0, :].values, dtype=torch.float32)
    writer_logs.add_graph(model, dummy_log_input)

    return writer_logs


@timecall(immediate=True)
def training_loop(criterion, model, optimizer, train_val_set, mse_criterion, writer_logs):
    # Training-val splits (k-fold cross validation)
    kf = KFold(n_splits=model.k_folds)
    kf.get_n_splits(train_val_set.inputs)
    i_fold = 0
    running_loss_val = 0.0
    running_loss_val_unscaled = 0.0
    best_num_epochs = 0

    for train_index, val_index in kf.split(train_val_set.inputs):
        model.apply(reinitialize_params)

        num_epochs = 1
        best_loss_kfold = float("inf")
        best_num_epochs_kfold = 0.0
        for epoch in range(num_epochs):  # loop over the train_val_set multiple times
            inputs = train_val_set.inputs[train_index, :]
            targets = train_val_set.targets[train_index, :]
            inputs, scaler_inputs = normalize_dataframe(inputs)
            targets, scaler_targets = normalize_dataframe(targets)

            model.train()
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model.forward(inputs)
            loss = nll_loss(targets, pi=outputs[0], mu=outputs[1], sigma=outputs[2])
            loss.backward()
            optimizer.step()

            # Validation
            with torch.set_grad_enabled(False):
                model.eval()
                inputs_val = train_val_set.inputs[val_index, :]
                targets_val = train_val_set.targets[val_index, :]
                inputs_val, scaler_inputs_val = normalize_dataframe(inputs_val)
                targets_val, scaler_targets_val = normalize_dataframe(targets_val)
                targets_val_n = targets_val.cpu().numpy()

                outputs_val = model.forward(inputs_val)
                loss_val = nll_loss(targets_val, pi=outputs_val[0], mu=outputs_val[1], sigma=outputs_val[2])
                loss_val = loss_val.detach().item()

                if loss_val < best_loss_kfold:
                    best_loss_kfold = loss_val
                    best_num_epochs_kfold = epoch

            if epoch % 50 == 0:
                #-----Tensorboard logs-----------------
                #tensorboard --logdir logs
                # 1. Log scalar values (scalar summary)

                writer_logs.add_scalars("Loss k-fold {}".format(i_fold),{
                    #"Train": running_loss,
                    "Cross validation": loss_val
                }, global_step=epoch)


                #-----------------------------------------
                print(
                    "Epoch {}; k-fold: {}; Loss (cross validation): {}; ".format(epoch,
                                                                                                                     i_fold,
                                                                                                                     loss_val,
                                                                                                                     ))
        running_loss_val += best_loss_kfold
        best_num_epochs += best_num_epochs_kfold
        i_fold += 1

    # Average of the loss in the all the k-folds cross validation:
    running_loss_val = running_loss_val / model.k_folds
    best_num_epochs = int(best_num_epochs / model.k_folds)
    # -----Tensorboard logs-----------------
    writer_logs.add_scalars("Loss all k-fold average", {
        "Cross validation": running_loss_val
    })

    print("Model cross validation finished")
    print("Average loss: ", running_loss_val)
    print("Best num epochs: ", best_num_epochs)
    print("======================================================")

    return running_loss_val, best_num_epochs

def train_whole_best_model(best_model_val, train_val_set, best_num_epochs, criterion, mse_criterion, writer_logs, cuda):
    best_model = NeuralNetwork(train_val_set.inputs.shape[1], train_val_set.targets.shape[1], best_model_val.num_hidden_layers,
                               best_model_val.neurons_per_hlayer, best_model_val.dropout, best_model_val.dropout_rate,
                               best_model_val.reg_l1, best_model_val.lr, cuda=cuda)
    optimizer = optim.Adam(best_model.parameters(), lr=best_model_val.lr)  # , momentum=0.09) #lr=0.003)
    print("----------------------------------------------------")
    print("Training best model with all the training + val instances")
    print("----------------------------------------------------")

    for epoch in range(best_num_epochs):  # loop over the train_val_set multiple times
        inputs = train_val_set.inputs
        targets = train_val_set.targets
        inputs, scaler_inputs = normalize_dataframe(inputs)
        targets, scaler_targets = normalize_dataframe(targets)

        best_model.train()

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = best_model.forward(inputs)
        loss = nll_loss(targets, pi=outputs[0], mu=outputs[1], sigma=outputs[2])
        loss.backward()
        optimizer.step()

        running_loss = loss.detach().item()


        #-----Tensorboard logs-----------------
        #tensorboard --logdir logs
        # 1. Log scalar values (scalar summary)
        writer_logs.add_scalars("Loss train+val", {
            "Train+val": running_loss,
        }, global_step=epoch)


        #-----------------------------------------
        print(
            "Epoch {}; Train loss: {};;".format(epoch,running_loss,
                                                                        ))

    return best_model

def test_best_model(best_model_path, test_set, specimen_ids, cols_electro, protocols_config_df):
    print("=====================================================")
    print("Starting testing in the test set...")

    #cols_neg_electro = get_electro_cols_neg_vals(cols_electro.tolist())

    #Train in the whole train set
    model = torch.load(best_model_path)
    model.eval()

    criterion = nn.MSELoss()
    mse_criterion = nn.MSELoss()
    writer_logs = SummaryWriter('logs/{}_test'.format(model.name))

    # Test in the train set (not used before)
    with torch.set_grad_enabled(False):
        inputs_test = test_set.inputs
        targets_test = test_set.targets
        inputs_test, scaler_inputs = normalize_dataframe(inputs_test)
        targets_test, scaler_targets = normalize_dataframe(targets_test)

        targets_test_n = targets_test.cpu().numpy()

        outputs_test = model.forward(inputs_test)
        #outputs_test = force_positive_values(outputs_test, cols_neg_electro)
        loss_test = nll_loss(targets_test, pi=outputs_test[0], mu=outputs_test[1], sigma=outputs_test[2])
        loss_test_item = loss_test.detach().item()
        running_loss_test = loss_test_item

        targets_test_unscaled = scaler_targets.inverse_transform(targets_test.cpu())
        outputs_test_pi = outputs_test[0].detach().cpu().numpy()
        outputs_test_mu_unscaled = scaler_targets.inverse_transform(outputs_test[1].detach().cpu())
        outputs_test_sigma_unscaled = np.abs(scaler_targets.inverse_transform(outputs_test[2].detach().cpu()))

        loss_test_unscaled = mse_criterion(torch.tensor(outputs_test_mu_unscaled), torch.tensor(targets_test_unscaled))
        running_loss_test_unscaled = loss_test_unscaled.detach().item()

    # -----Tensorboard logs-----------------
    # tensorboard --logdir logs
    writer_logs.add_scalars("Loss", {
        "Test": running_loss_test,
    })

    print("=====================================================")
    print("Test set loss: ", running_loss_test)
    print("Test set loss unscaled: ", running_loss_test_unscaled)

    print("Pi: {}".format(outputs_test_pi))
    print("Mean: {}".format(outputs_test_mu_unscaled))
    print("Variance: {}".format(outputs_test_sigma_unscaled))
    print("=====================================================")


    for i in range(min(2, targets_test_unscaled.shape[0])):
        pi = outputs_test_pi[i, :]
        mu = outputs_test_mu_unscaled[i, :]
        sigma = outputs_test_mu_unscaled[i, :]

        x = np.linspace(mu.min()-4*sigma.max(), mu.max()+4*sigma.max(), 1000)
        y_total = np.zeros_like(x)
        traces_pdf = []
        for j in range(len(pi)):
            pi_component = pi[j].item()
            mu_component = mu[j].item()
            sigma_component = sigma[j].item()

            y = scipy_stats.norm.pdf(x, mu_component, sigma_component) * pi_component
            y_total += y
            prob_y = np.trapz(y / pi_component, x=x)
            print("Prob (integral) gaussian i: ", prob_y)

            plot_name = "GMM_{}".format(i)
            trace_pdf = plotly_graph.Scatter(
                x=x,
                y=y,
                mode='lines',
                name="Gaussian {}".format(j),
            )
            traces_pdf.append(trace_pdf)

        prob_total = np.trapz(y_total, x=x)
        print("Prob (integral) GMM: ", prob_total)

        trace_pdf = plotly_graph.Scattergl(
            x=x,
            y=y_total,
            mode='lines',
            name="GMM result",
            line=dict(width=4),
        )
        traces_pdf.append(trace_pdf)

        layout = utils.plotly_set_layout(title=plot_name, column_x_name="Num neighbors",
                                   column_y_name="Num nodes", all_x_labels=0)
        figure = plotly_graph.Figure(data=traces_pdf, layout=layout)
        plotly.offline.plot(figure, filename='{}.html'.format(plot_name))

    # Save targets predicted:


    return targets_original_df, targets_predicted_mu_df, targets_predicted_sigma_df


def test_random_model(test_set, num_hlayers, neurons_per_hlayer, cuda):
    print("=====================================================")
    print("Starting test random model in the test set...")

    #Train in the whole train set
    model = NeuralNetwork(test_set.inputs.shape[1], test_set.targets.shape[1], num_hlayers, neurons_per_hlayer, False, cuda=cuda)
    model.eval()

    criterion = nn.MSELoss()
    mse_criterion = nn.MSELoss()
    writer_logs = SummaryWriter('logs/{}_test'.format(model.name))

    # Test in the train set (not used before)
    with torch.set_grad_enabled(False):
        inputs_test = test_set.inputs
        targets_test = test_set.targets
        inputs_test, scaler_inputs = normalize_dataframe(inputs_test)
        targets_test, scaler_targets = normalize_dataframe(targets_test)
        targets_test_n = targets_test.cpu().numpy()

        outputs_test = model.forward(inputs_test)
        loss_test = nll_loss(targets_test, pi=outputs_test[0], mu=outputs_test[1], sigma=outputs_test[2])
        loss_test_item = loss_test.detach().item()
        running_loss_test = loss_test_item

        outputs_test_unscaled = scaler_targets.inverse_transform(outputs_test[0].detach().cpu())
        targets_test_unscaled = scaler_targets.inverse_transform(targets_test.cpu())
        loss_test_unscaled = mse_criterion(torch.tensor(outputs_test_unscaled), torch.tensor(targets_test_unscaled))
        running_loss_test_unscaled = loss_test_unscaled.detach().item()

    # -----Tensorboard logs-----------------
    # tensorboard --logdir logs
    writer_logs.add_scalars("Loss", {
        "Test": running_loss_test,
    })


    print("=====================================================")
    print("Test set random loss: ", running_loss_test)
    print("Test set random loss unscaled: ", running_loss_test_unscaled)
    print("=====================================================")

    return running_loss_test

def plotly_set_layout(title="", column_x_name="", column_y_name="", all_x_labels=1, height=600):

    layout = plotly_graph.Layout(
        title = title,
        autosize=True,
        height=height,
        margin=plotly_graph.layout.Margin(
        ),
        xaxis=dict(
            title = column_x_name,
            automargin= True,
            dtick=all_x_labels,
        ),
        yaxis=dict(
            title = column_y_name,
            automargin=True,
        ),
    )

    return layout

def get_electro_cols_neg_vals(electro_df_columns):
    electro_cols_neg_vals = ['min_voltage_between_spikes', 'steady_state_voltage_stimend', 'voltage_after_stim']
    if not any((True for x in electro_cols_neg_vals if x in electro_df_columns)):
        electro_cols_neg_vals = [col for col in electro_df_columns if col.split('__')[1] in electro_cols_neg_vals]

    electro_cols_neg_vals = [electro_df_columns.index(col) for col in electro_cols_neg_vals]

    return electro_cols_neg_vals

def force_positive_values(matrix, cols_neg):
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            if j not in cols_neg and matrix[i, j] < 0:
                matrix[i, j] = 0.0

    return matrix

def calc_MI(x, y, bins):
    c_xy = np.histogram2d(x, y, bins)[0]
    mi = mutual_info_score(None, None, contingency=c_xy)
    return mi

def compute_mi_matrix(df, load_csv=False):
    filename_csv = "./feature_selection_data/mi_matrix.csv"
    col_id = "features"

    if load_csv:
        mi_matrix_pd = pd.read_csv(filename_csv)
        mi_matrix_pd.set_index(col_id,  inplace=True)
    else:
        n = df.shape[1]
        mi_matrix = np.zeros((n, n), dtype=np.float64)
        for i in range(n):
            print("MI ", i)
            for j in range(n):
                if i != j:
                    mi = calc_MI(df.iloc[:, i], df.iloc[:, j], 10)
                    mi_matrix[i, j] = mi

        mi_matrix_pd = pd.DataFrame(mi_matrix, columns=df.columns.values)
        mi_matrix_pd["features"] = df.columns.values
        cols = mi_matrix_pd.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        mi_matrix_pd = mi_matrix_pd[cols]
        mi_matrix_pd.set_index(col_id)

        mi_matrix_pd.to_csv(filename_csv, index=False)

    return mi_matrix_pd

def compute_correlation_matrix(df, load_csv=False):
    filename_csv = "./feature_selection_data/correlation_matrix.csv"
    col_id = "features"

    if load_csv:
        correlation_matrix = pd.read_csv(filename_csv)
        correlation_matrix.set_index(col_id, inplace=True)
    else:
        correlation_matrix = df.corr()
        correlation_matrix = correlation_matrix.rename_axis(col_id)
        correlation_matrix.to_csv(filename_csv, index=True)

    return correlation_matrix

def feature_selection(morpho_df_original, electro_df):
    specimen_ids = morpho_df_original.loc[:, ["specimen_id"]]
    morpho_df = morpho_df_original.drop(columns=["specimen_id"])
    all_data_df = pd.concat([morpho_df, electro_df], axis=1)
    #all_data.to_csv("./all_data.csv")

    mi_matrix = compute_mi_matrix(all_data_df, load_csv=True)
    correlation_matrix = compute_correlation_matrix(all_data_df, load_csv=True)

    cols_morpho = morpho_df.columns.values
    cols_electro = electro_df.columns.values

    stimulus_amplitudes = [0.17, 0.4, 0.5, 0.9, 1.5]
    sections = ["soma", "dend", "apic", "axon"]
    pos_sections = ["closest", "farthest"]

    list_mi_sum = []
    list_correlation_sum = []
    for amp in stimulus_amplitudes:
        for iclamp_sect in sections:
            for iclamp_pos in pos_sections:
                for record_sect in sections:
                    for record_pos in pos_sections:
                        valid = AllenSDK.is_valid_stimulus(iclamp_sect, iclamp_pos, record_sect, record_pos)
                        if not valid:
                            continue
                        col_name = "{}_{}_{}_{}_amp_{}".format(iclamp_sect, iclamp_pos, record_sect, record_pos, amp)
                        electro_df_cols = [col for col in cols_electro if col.startswith(col_name)]
                        electro_df_this = electro_df.loc[:, electro_df_cols]

                        mi_matrix_this = mi_matrix.loc[cols_morpho, electro_df_cols]
                        mi_matrix_this = mi_matrix_this.apply(lambda col: col.fillna(0), axis=0)
                        correlation_matrix_this = correlation_matrix.loc[cols_morpho, electro_df_cols]
                        correlation_matrix_this = correlation_matrix_this.apply(lambda col: col.fillna(0), axis=0)

                        sum_mi_matrix = mi_matrix_this.values.sum()
                        sum_correlation_matrix = correlation_matrix_this.values.sum()
                        list_mi_sum.append((col_name, sum_mi_matrix, mi_matrix_this.sum(axis=1), mi_matrix_this.sum(axis=0)))
                        list_correlation_sum.append((col_name, sum_correlation_matrix, correlation_matrix_this.sum(axis=1), correlation_matrix_this.sum(axis=0)))

    list_mi_sum = sorted(list_mi_sum, key=itemgetter(1), reverse=True)
    list_correlation_sum = sorted(list_correlation_sum, key=itemgetter(1), reverse=True)

    cols_morpho_mi_mean = pd.DataFrame([item[2] for item in list_mi_sum]).mean(axis=0).sort_values(ascending=False)
    cols_morho_corr_mean = pd.DataFrame([item[2] for item in list_correlation_sum]).mean(axis=0).sort_values(ascending=False)

    cols_electro_mi_mean = pd.DataFrame([item[3] for item in list_mi_sum]).mean(axis=0).sort_values(ascending=False)
    cols_electro_mi_mean.index = [col.split("__")[1] for col in cols_electro_mi_mean.index]
    cols_electro_mi_mean = cols_electro_mi_mean.groupby(cols_electro_mi_mean.index).mean()

    cols_electro_corr_mean = pd.DataFrame([item[3] for item in list_correlation_sum]).mean(axis=0).sort_values(ascending=False)
    cols_electro_corr_mean.index = [col.split("__")[1] for col in cols_electro_corr_mean.index]
    cols_electro_corr_mean = cols_electro_corr_mean.groupby(cols_electro_corr_mean.index).mean()

    # Feature selection:
    percentile_to_remove_morho = 50
    percentile_rank_mi = np.percentile(cols_morpho_mi_mean.values, percentile_to_remove_morho)
    cols_morpho_remove_mi = set(cols_morpho_mi_mean[cols_morpho_mi_mean <= percentile_rank_mi].index.tolist())
    percentile_rank_corr = np.percentile(cols_morho_corr_mean.values, percentile_to_remove_morho)
    cols_morpho_remove_corr = set(cols_morho_corr_mean[cols_morho_corr_mean <= percentile_rank_corr].index.tolist())

    percentile_to_remove_electro = 50
    percentile_rank_mi = np.percentile(cols_electro_mi_mean.values, percentile_to_remove_electro)
    cols_electro_remove_mi = set(cols_electro_mi_mean[cols_electro_mi_mean <= percentile_rank_mi].index.tolist())
    percentile_rank_corr = np.percentile(cols_electro_corr_mean.values, percentile_to_remove_electro)
    cols_electro_remove_corr = set(cols_electro_corr_mean[cols_electro_corr_mean <= percentile_rank_corr].index.tolist())

    cols_morpho_remove = cols_morpho_remove_mi & cols_morpho_remove_corr
    cols_electro_remove = cols_electro_remove_mi & cols_electro_remove_corr

    # Stimuli iclamp and record selection:
    num_electro_configs = 3
    best_electro_configs = [electro_config[0] for electro_config in list_mi_sum[0:num_electro_configs]]
    best_electro_configs = ['apic_closest_soma_closest_amp_0.9', 'apic_closest_soma_closest_amp_0.5', 'dend_closest_dend_farthest_amp_0.9',  'dend_closest_dend_farthest_amp_0.5', 'dend_closest_soma_closest_amp_0.4']
    '''
    best with apic: 'apic_closest_soma_closest_amp_0.9' 'apic_closest_soma_closest_amp_0.5'
    best without apic: 'dend_closest_dend_farthest_amp_0.9'  'dend_closest_dend_farthest_amp_0.5' 'dend_closest_soma_closest_amp_0.4'
    '''
    print("====== Electro configs: {}========".format(best_electro_configs))

    cols_electro_config = []
    for col in cols_electro:
        for electro_config in best_electro_configs:
            if col.startswith(electro_config):
                cols_electro_config.append(col)
    cols_electro_config = [col for col in cols_electro_config if col.split('__')[1] not in cols_electro_remove]
    #cols_electro_get = ["Spikecount"]
    #cols_electro_config = [col for col in cols_electro_config if col.split('__')[1] in cols_electro_get]

    morpho_df = morpho_df.loc[:, set(cols_morpho) - cols_morpho_remove]

    #cols_electro_config = ['']
    electro_df = electro_df.loc[:, cols_electro_config]

    morpho_df["specimen_id"] = specimen_ids

    #Create protocols_config_df
    protocols_config = []
    cols_protocols = ["iclamp_sect", "iclamp_pos", "record_sect", "record_pos", "amp", "delay", "duration"]
    for electro_config in best_electro_configs:
        protocol = {
            "name": electro_config,
            "iclamp_sect": electro_config.split("_")[0],
            "iclamp_pos": electro_config.split("_")[1],
            "record_sect": electro_config.split("_")[2],
            "record_pos": electro_config.split("_")[3],
            "amp": electro_config.split("_")[-1],
            "delay": 50,
            "duration": 200
        }
        protocols_config.append(protocol)

    protocols_config_df = pd.DataFrame(protocols_config)

    '''
    electro_df = electro_df.loc[(electro_df != 0).all(axis=1)]
    morpho_df = morpho_df.reindex(electro_df.index)
    electro_df = electro_df.reset_index(drop=True)
    morpho_df = morpho_df.reset_index(drop=True)
    '''
    return morpho_df, electro_df, protocols_config_df

if __name__ == '__main__':
    #dataframe = pd.read_parquet("data/clean_full_brain.parquet.gzip", engine="fastparquet")
    morpho_df_ = pd.read_csv("../../input_data/preprocessed_morpho_biophys_models.csv", na_filter=False, low_memory=False)
    #metadata_df = pd.read_csv("../../input_data/preprocessed_metadata_biophys_models.csv", na_filter=False, low_memory=False)
    electro_df = pd.read_csv("../../input_data/preprocessed_electro_biophys_models_efel.csv", na_filter=False, low_memory=False)

    #morpho_df = pd.concat([morpho_df, metadata_df], axis=1) #Morpho+metadata predictor features

    #morpho_df = morpho_df.loc[:, ["total_length", "total_surface", "total_volume"]]
    #electro_df = electro_df.loc[:, ["dend_apical_closest_dend_apical_closest_amp_0.17__AP_height"]]

    #Completely correlated features example:
    morpho_df = morpho_df_.loc[:, ["specimen_id", "number_tips"]]
    electro_df = morpho_df_.loc[:, ["number_bifurcations"]] #"number_bifurcations"

    """
    file_name_merged = "./dataframe_merged.csv"
    dataframe_merged = pd.concat([morpho_df, electro_df], axis=1)
    dataframe_merged["id"] = dataframe_merged.index
    cols = dataframe_merged.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    dataframe_merged = dataframe_merged[cols]
    dataframe_merged.to_csv(file_name_merged, index=False)
    """
    protocols_config_df = {}
    #morpho_df, electro_df, protocols_config_df = feature_selection(morpho_df, electro_df)
    best_model_path, train_val_set, test_set, specimen_ids_test = training(morpho_df, electro_df, cuda=False)
    target_original_df, target_predicted_mu_df, target_predicted_sigma_df = test_best_model(best_model_path, test_set, specimen_ids_test, electro_df.columns.values, protocols_config_df)
