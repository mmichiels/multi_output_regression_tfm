import numpy as np
import pandas as pd
import os


def preprocess_morpho(morpho_df, drop_col_id=True):
    col_id = "specimen_id"
    cols_to_drop = ["Unnamed: 0", "average_bifurcation_angle_remote", "hausdorff_dimension", "id",
                    "superseded", "tags", "neuron_reconstruction_type"]
    if drop_col_id:
        cols_to_drop.append(col_id)

    morpho_df = preprocess_for_ml(morpho_df, col_id, cols_to_drop, cols_strings=[], continuous_data=True)

    return morpho_df

def preprocess_electro_allen_website(electro_df):
    col_id = "specimen_id"

    cols_to_drop = ["Unnamed: 0",
                    'electrode_0_pa', #Metadata from Test Sweeps
                    'input_resistance_mohm', #Metadata from Test Sweeps
                    "id",
                    "rheobase_sweep_id", #Sweep numbers aren't revelant, only average data from all sweeps
                    "rheobase_sweep_number", #Sweep numbers aren't revelant, only average data from all sweeps
                    "ri", #ri = Rinput = Resistance input? Is this the Rinput Quality control in the ephys white paper?
                    "seal_gohm", # Metadata from Test Sweeps
                    "thumbnail_sweep_id", #Not documented? #Sweep numbers aren't revelant, only average data from all sweeps
                    'has_burst',
                    'has_delay',
                    'has_pause',
                    col_id,
                    ]


    cols_useful = [
        "sag" #fraction that membrane potential relaxes back to baseline https://allensdk.readthedocs.io/en/v0.14.5/allensdk.ephys.ephys_extractor.html
        "tau" #membrane time constant in seconds https://allensdk.readthedocs.io/en/v0.14.5/allensdk.ephys.ephys_extractor.html
    ]

    electro_df = preprocess_for_ml(electro_df, col_id, cols_to_drop, cols_strings=[], continuous_data=True)

    return electro_df

def preprocess_metadata(metadata_df, drop_col_id=True):
    col_id = "id"
    cols_to_drop = ["Unnamed: 0", "apical", "cell_soma_location", "donor_id", "name", "normalized_depth", "reconstruction_type", "reporter_status",
                    "species"]
    cols_strings = ["dendrite_type", "disease_state", "structure_area_abbrev", "structure_area_id", "structure_layer_name",
                    "structure_hemisphere", "transgenic_line"]

    if drop_col_id:
        cols_to_drop.append(col_id)

    metadata_df = preprocess_for_ml(metadata_df, col_id, cols_to_drop, cols_strings, continuous_data=True)

    return metadata_df

def preprocess_electro_efel(electro_df, drop_col_id=True):
    col_id = "specimen_id"
    cols_to_drop = []
    if drop_col_id:
        cols_to_drop.append(col_id)

    cols_neg_vals = ['min_voltage_between_spikes', 'steady_state_voltage_stimend', 'voltage_after_stim']
    if not any((True for x in cols_neg_vals if x in electro_df.columns)):
        cols_neg_vals = [col for col in electro_df.columns.values if col != col_id and col.split('__')[1] in cols_neg_vals]

    electro_df = preprocess_for_ml(electro_df, col_id, cols_to_drop, cols_strings=[], continuous_data=True,
                                   nans="zeros", cols_neg_vals=cols_neg_vals)

    return electro_df

def preprocess_electro_allensdk(electro_df):
    col_id = "specimen_id"
    cols_to_drop = [
                    col_id,
                    ]

    electro_df = preprocess_for_ml(electro_df, col_id, cols_to_drop, cols_strings=[], continuous_data=True)

    return electro_df

def preprocess_morpho_electro_metadata_biophys_models(morpho_df, electro_biophys_models_df, metadata_df, drop_col_id=False):
    col_id = "specimen_id"
    specimen_ids_morpho = np.array(morpho_df.loc[:, col_id].values, dtype=np.int64)
    electro_biophys_models_df = electro_biophys_models_df.loc[electro_biophys_models_df[col_id].isin(specimen_ids_morpho)] #Get only morpho specimen_ids that are in the electro dataframe
    specimen_ids_electro = np.array(electro_biophys_models_df.loc[:, col_id].values, dtype=np.int64)
    morpho_df = morpho_df.loc[morpho_df[col_id].isin(specimen_ids_electro)] #Get only morpho specimen_ids that are in the electro dataframe

    metadata_col_id = "id"
    metadata_df = metadata_df.loc[metadata_df[metadata_col_id].isin(specimen_ids_electro)] #Get only morpho specimen_ids that are in the electro dataframe

    metadata_df = metadata_df.drop(columns=[metadata_col_id])
    electro_biophys_models_df = electro_biophys_models_df.drop(columns=[col_id])
    if drop_col_id:
        morpho_df = morpho_df.drop(columns=[col_id])

    return morpho_df, electro_biophys_models_df, metadata_df


def preprocess_for_ml(df, col_id=None, cols_to_drop=[], cols_strings=[], continuous_data=True, nans="mean", cols_neg_vals=None):
    df = df.loc[:, ~df.columns.duplicated()]
    df.dropna(how='all', axis=1, inplace=True) #Delete empty columns
    df.drop_duplicates(keep=False, inplace=True)
    if col_id is not None:
        df.drop_duplicates(subset=col_id, keep=False, inplace=True)
        df.sort_values([col_id], axis=0, inplace=True)
    df.reset_index(drop=True, inplace=True)

    #Drop manually selected columns:
    for col in cols_to_drop: #We loop over them to check if they exist
        if col in df.columns:
            df = df.drop(columns=[col])

    #Drop invariant columns
    df = df.loc[:, (df != df.iloc[0]).any()]

    # Replace categories string values by numbers to fit with the algorithm input:
    for col in cols_strings:
        if col in df.columns:
            column = df.loc[:, col]
            column = column.astype('category')
            df.loc[:, col] = column.cat.codes

    #Infer nans to be the mean of that column
    if nans == "zeros":
        df = df.apply(lambda col: col.fillna(0), axis=0)
    elif nans == "mean":
        df = df.apply(lambda col: col.fillna(col.mean()), axis=0)

    if continuous_data:
        df = df.astype(np.float64)

    if cols_neg_vals is not None:
        for col in df.columns:
            if col != col_id and col not in cols_neg_vals:
                df.loc[df[col] < 0, col] = 0

    return df

if __name__ == '__main__':
    input_data_path = "../../input_data"
    morpho_df = pd.read_csv(os.path.join(input_data_path, "raw_morpho.csv"), low_memory=False)
    electro_website_df = pd.read_csv(os.path.join(input_data_path, "raw_electro_website.csv"), low_memory=False)
    metadata_df = pd.read_csv(os.path.join(input_data_path, "raw_metadata.csv"), low_memory=False)
    electro_efel_df = pd.read_csv(os.path.join(input_data_path, "raw_electro_efel.csv"), low_memory=False)
    electro_allensdk_df = pd.read_csv(os.path.join(input_data_path, "raw_electro_allensdk.csv"), low_memory=False)
    electro_biophys_models_efel_df = pd.read_csv(os.path.join(input_data_path, "raw_biophys_models_electro_efel.csv"), low_memory=False)

    morpho_df_with_col_id = preprocess_morpho(morpho_df, drop_col_id=False)
    morpho_df = preprocess_morpho(morpho_df)
    electro_website_df = preprocess_electro_allen_website(electro_website_df)
    metadata_df_with_col_id = preprocess_metadata(metadata_df, drop_col_id=False)
    metadata_df = preprocess_metadata(metadata_df)
    electro_efel_df = preprocess_electro_efel(electro_efel_df)
    electro_allensdk_df = preprocess_electro_allensdk(electro_allensdk_df)
    electro_biophys_models_efel_df = preprocess_electro_efel(electro_biophys_models_efel_df, drop_col_id=False)
    morpho_biophys_models, electro_biophys_models_efel_df, metadata_biophys_df = preprocess_morpho_electro_metadata_biophys_models(
        morpho_df_with_col_id, electro_biophys_models_efel_df, metadata_df_with_col_id, drop_col_id=False)

    morpho_df.to_csv(os.path.join(input_data_path, "preprocessed_morpho.csv"), index=False)
    electro_website_df.to_csv(os.path.join(input_data_path, "preprocessed_electro_website.csv"), index=False)
    metadata_df.to_csv(os.path.join(input_data_path, "preprocessed_metadata.csv"), index=False)
    electro_efel_df.to_csv(os.path.join(input_data_path, "preprocessed_electro_efel.csv"), index=False)
    electro_allensdk_df.to_csv(os.path.join(input_data_path, "preprocessed_electro_allensdk.csv"), index=False)

    morpho_biophys_models.to_csv(os.path.join(input_data_path, "preprocessed_morpho_biophys_models.csv"), index=False)
    electro_biophys_models_efel_df.to_csv(os.path.join(input_data_path, "preprocessed_electro_biophys_models_efel.csv"), index=False)
    metadata_biophys_df.to_csv(os.path.join(input_data_path, "preprocessed_metadata_biophys_models.csv"), index=False)
