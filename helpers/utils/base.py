import math
from functools import wraps
import warnings
import plotly.graph_objs as plotly_graph
import plotly.tools as plotly_tools

def ignore_warnings(f):
    @wraps(f)
    def inner(*args, **kwargs):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("ignore")
            response = f(*args, **kwargs)
        return response
    return inner

def plotly_set_layout(title="", column_x_name="", column_y_name="", all_x_labels=1, height=600):

    layout = plotly_graph.Layout(
        title = title,
        autosize=True,
        height=height,
        margin=plotly_graph.layout.Margin(
        ),
        xaxis=dict(
            title = column_x_name,
            automargin= True,
            dtick=all_x_labels,
        ),
        yaxis=dict(
            title = column_y_name,
            automargin=True,
        ),
    )

    return layout

def get_subplots_rows_cols(shape, num_cols=3, titles=[]):
    rows = int(math.ceil(shape / num_cols))

    figure = plotly_tools.make_subplots(rows=rows, cols=num_cols, subplot_titles=titles, print_grid=False)
    counter_row = 1
    counter_col = 1

    return figure, counter_row, counter_col, rows

def get_subplot_counter_row_col(counter_row=1, counter_col=1, num_cols=3):
    counter_col += 1
    if counter_col > num_cols:
        counter_col = 1
        counter_row += 1

    return counter_row, counter_col
