import setuptools
from setuptools.command.install import install
import subprocess
import json

# Cloning Repos
print("Cloning PyGeppetto...")
subprocess.call(['git', 'clone', '-b', 'development', 'https://github.com/openworm/pygeppetto.git'], cwd='../')
subprocess.call(['pip', 'install', '-e', '.'], cwd='../pygeppetto/')

print("Cloning Geppetto Jupyter (Python package)...")
subprocess.call(['git', 'clone', '--recursive', '-b', 'master', 'https://github.com/openworm/org.geppetto.frontend.jupyter.git'], cwd='../')
subprocess.call(['git', 'checkout', 'tags/v0.4.1-alpha'], cwd='../org.geppetto.frontend.jupyter')

print("Cloning Geppetto Frontend")
subprocess.call(['git', 'clone', '--recursive', '-b', 'master', 'https://github.com/openworm/org.geppetto.frontend.git'], cwd='../')
subprocess.call(['git', 'checkout', '6dc3a60635d450e358c5486adcf33b715dcbbe16'], cwd='../org.geppetto.frontend/')
subprocess.call(['cp', '-R', '../org.geppetto.frontend/src/', '../org.geppetto.frontend.jupyter/src/jupyter_geppetto/geppetto'])


print("Cloning Geppetto Neuron Configuration ...")
subprocess.call(['git', 'clone', 'https://github.com/MetaCell/geppetto-neuron.git'],
                cwd='../org.geppetto.frontend.jupyter/src/jupyter_geppetto/geppetto/src/main/webapp/extensions/')

subprocess.call(['git', 'checkout', 'interface'], cwd='../org.geppetto.frontend.jupyter/src/jupyter_geppetto/geppetto/src/main/webapp/extensions/geppetto-neuron/')

print("Enabling Geppetto Neuron Extension ...")
jsonFile = open(
    '../org.geppetto.frontend.jupyter/src/jupyter_geppetto/geppetto/src/main/webapp/extensions/extensionsConfiguration.json',
    "w+")
jsonFile.write(json.dumps({"geppetto-neuron/ComponentsInitialization": True}))
jsonFile.close()

# Installing and building
print("NPM Install and build for Geppetto Frontend  ...")
subprocess.call(['npm', 'install'], cwd='../org.geppetto.frontend.jupyter/src/jupyter_geppetto/geppetto/src/main/webapp/')
subprocess.call(['npm', 'run', 'build-dev-noTest'], cwd='../org.geppetto.frontend.jupyter/src/jupyter_geppetto/geppetto/src/main/webapp/')

print("Installing jupyter_geppetto python package ...")
subprocess.call(['pip', 'install', '-e', '.'], cwd='../org.geppetto.frontend.jupyter')
print("Installing jupyter_geppetto Jupyter Extension ...")
subprocess.call(['jupyter', 'nbextension', 'install', '--py', '--symlink', '--user', 'jupyter_geppetto'], cwd='../org.geppetto.frontend.jupyter')
subprocess.call(['jupyter', 'nbextension', 'enable', '--py', '--user', 'jupyter_geppetto'], cwd='../org.geppetto.frontend.jupyter')
subprocess.call(['jupyter', 'nbextension', 'enable', '--py', 'widgetsnbextension'], cwd='../org.geppetto.frontend.jupyter')
subprocess.call(['jupyter', 'serverextension', 'enable', '--py', 'jupyter_geppetto'], cwd='../org.geppetto.frontend.jupyter')

print("Installing neuron_ui python package ...")
subprocess.call(['pip', 'install', '-e', '.'], cwd='..')
