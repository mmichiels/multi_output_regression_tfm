class ModelInterpreter():

    def __init__(self):
        pass

    def importType(self, url, typeName, library, commonLibraryAccess):
        pass

    def importValue(self, importValue):
        pass

    def downloadModel(self, pointer, format, aspectConfiguration):
        pass

    def getSupportedOutputs(self, pointer):
        pass

    def getName(self):
        pass

    def getDependentModels(self):
        pass