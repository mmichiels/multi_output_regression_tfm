import logging
import os

import neuron
from neuron import h, gui
from IPython.core.debugger import Tracer
from neuron_ui import neuron_utils, neuron_geometries_utils
from math import sqrt, pow
import pkg_resources

from jupyter_geppetto.geppetto_comm import GeppettoCoreAPI as G
from jupyter_geppetto.geppetto_comm import GeppettoJupyterModelSync
import json

class PTcell:

    def __init__(self):
        logging.debug("testing Custom neuron :::)")
        logging.debug(neuron.version)

        neuron_utils.createProject(name='PTcell')

        self.load_morphology()
        self.load_mechanisms_and_parameters()
        self.define_record_variables()

        neuron_geometries_utils.extractGeometries()

        done = True

    def load_morphology(self):
        h.load_file('stdrun.hoc')
        h.load_file('import3d.hoc')
        morph_filename = 'reconstruction.swc'
        resource_package = "neuron_ui"
        resource_path = '/'.join(('models', morph_filename))  #
        morph_filepath = pkg_resources.resource_filename(resource_package, resource_path)
        my_swc = h.Import3d_SWC_read(morph_filepath)
        my_swc.input(morph_filepath)
        imprt = h.Import3d_GUI(my_swc, 0)
        h("objref this")
        imprt.instantiate(h.this)

        self.fix_soma_axon()
        h.define_shape()
        print(h.topology())

        return 0

    def fix_soma_axon(self):
        h("soma[0] area(0.5)")
        for sec in h.allsec():
            sec.nseg = 1 + 2 * int(sec.L / 40.0)
            if sec.name()[:4] == "axon":
                h.delete_section(sec=sec)
        h('create axon[2]')
        for sec in h.axon:
            sec.L = 30
            sec.diam = 1
            sec.nseg = 1 + 2 * int(sec.L / 40.0)
        h.axon[0].connect(h.soma[0], 0.5, 0.0)
        h.axon[1].connect(h.axon[0], 1.0, 0.0)

        return 0

    def load_mechanisms_and_parameters(self):
        config_dir = 'config'
        resource_package = "neuron_ui"
        mechs_and_params_filename = os.path.join(config_dir, 'mechanisms_and_parameters.json')
        resource_path = '/'.join(('models', mechs_and_params_filename))
        mechs_and_params_filepath = pkg_resources.resource_filename(resource_package, resource_path)
        mechs_and_params = json.load(open(mechs_and_params_filepath))

        for sectionlist, channels in mechs_and_params["mechanisms"].items():
            for channel in channels:
                logging.debug('Adding mechanism {} to {}'.format(channel, sectionlist))
                if sectionlist == "all":
                    all_sectionlist = h.allsec()
                else:
                    all_sectionlist = getattr(h, sectionlist)
                for section in all_sectionlist:
                    if h.ismembrane(str(channel), sec=section) != 1:
                        section.insert(channel)

        for sectionlist, params in mechs_and_params["parameters"].items():
            for name, value in params.items():
                logging.debug('Adding parameter {} ({}) to {}'.format(name, value, sectionlist))
                if sectionlist == "all":
                    all_sectionlist = h.allsec()
                else:
                    all_sectionlist = getattr(h, sectionlist)
                for section in all_sectionlist:
                    setattr(section, name, value)

        return 0

    def define_record_variables(self):
        # record soma voltage and time
        self.soma = h.soma[0]
        self.dend = h.apic

        self.t_vec = h.Vector()
        self.t_vec.record(h._ref_t)
        neuron_utils.createStateVariable(id='time', name='time',
                                         units='ms', python_variable={"record_variable": self.t_vec,
                                                                      "segment": None})
        # run simulation
        h.tstop = 400  # ms

        return 0

    def analysis(self):
        # plot voltage vs time
        self.plotWidget = G.plotVariable(
            'Plot', ['SimpleCell.v_vec_dend', 'SimpleCell.v_vec_soma'])

    def analysis_matplotlib(self):
        from matplotlib import pyplot
        pyplot.figure(figsize=(8, 4))  # Default figsize is (8,6)
        pyplot.plot(self.t_vec, self.v_vec_soma, label='soma')
        pyplot.plot(self.t_vec, self.v_vec_dend, 'r', label='dend')
        pyplot.xlabel('time (ms)')
        pyplot.ylabel('mV')
        pyplot.legend()
        pyplot.show()

if __name__ == "__main__":
    PTcell()
