\beamer@endinputifotherversion {3.36pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Objetivos}{3}{0}{1}
\beamer@sectionintoc {2}{Data set}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Variables}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Preprocesado de los datos}{11}{0}{2}
\beamer@sectionintoc {3}{Machine learning}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Redes neuronales}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{Regresi\IeC {\'o}n multi-output con incertidumbre en la salida}{16}{0}{3}
\beamer@sectionintoc {4}{Resultados}{17}{0}{4}
\beamer@subsectionintoc {4}{1}{Medidas (MSE, NLL)}{17}{0}{4}
\beamer@subsectionintoc {4}{2}{Simulaci\IeC {\'o}n de modelos biof\IeC {\'\i }sicos}{21}{0}{4}
\beamer@sectionintoc {5}{Discusi\IeC {\'o}n}{23}{0}{5}
