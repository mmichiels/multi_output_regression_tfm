import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats as stats
import math
import seaborn as sns


def plot_benchmark(kind="bar"): #Type: bar, point
    title = "benchmark_a_mse"

    sns.set_style(style="whitegrid")

    rotation = 20
    if title.startswith("benchmark_a"):
        plot_title = "Group A results"
    else:
        plot_title = "Group B results"
    if title.endswith("mse"):
        y_col = "MSE"
        plot_title += " (MSE)"
        rotation_ha = "right"
        fontsize = 21
        plt.rcParams['figure.figsize'] = 10,8
        fontsize_title = fontsize + 3
    else:
        y_col = "NLL"
        plot_title += " (NLL)"
        rotation_ha = "center"
        fontsize = 21
        plt.rcParams['figure.figsize'] = 10, 8
        fontsize_title = fontsize + 3

    dataframe = pd.read_csv("{}.csv".format(title), na_filter=False, low_memory=False)

    x_col = "Algorithm"
    g = sns.barplot(x=x_col, y=y_col, hue="Set", data=dataframe)
    #g.despine(left=True)
    #max = 26
    #yticks = [str(label) for label in range(max)] + ["> {}".format(max)]
    #g.set(yticklabels=yticks)
    x_labels = list((dataframe.loc[:, "Algorithm"].values))
    if rotation:
        g.set_xticklabels(x_labels, rotation=rotation, ha=rotation_ha, fontsize=fontsize)
    else:
        g.set_xticklabels(x_labels, fontsize=fontsize)

    g.set_xlabel(x_col, fontsize=fontsize_title)
    g.set_ylabel(y_col, fontsize=fontsize_title)
    g.tick_params(labelsize=fontsize)
    if title.endswith("mse"):
        plt.legend(fontsize=fontsize)
    else:
        plt.legend(fontsize=fontsize, bbox_to_anchor=(1, 1))

    #plt.yticks(np.arange(0, max+1, 1.0))
    plt.title(plot_title, fontsize=fontsize_title)
    plt.tight_layout()
    #plt.show()
    plt.savefig("{}.png".format(title), format="png")

def plot_normal_distribution():
    sns.set(rc={'figure.figsize': (4.7, 3.27)})

    mu = 0
    variance = 1
    sigma = math.sqrt(variance)
    x = np.linspace(mu - 3 * sigma, mu + 3 * sigma, 100)
    plt.plot(x, stats.norm.pdf(x, mu, sigma), c='b', linewidth=5.0)
    plt.axis('off')
    plt.tight_layout()
    plt.savefig("normal_distribution.png", format="png")

if __name__ == '__main__':
    plot_benchmark()
    #plot_normal_distribution()



