import io
import json
import logging
import os
import zipfile
import base64

import time
import datetime
from random import randint
from helpers import io as biophys_io

# Create your views here.

# ----Index----------

def load_morpho_electro_allen(request):
    data = {}
    query_filters = json.loads(request.POST.get("query-values-json", "{}"))
    custom_filters = json.loads(request.POST.get("custom-options-json", "{}"))

    allensdk = biophys_io.AllenSDK(request.session.session_key)

    morpho_df, electro_df = allensdk.get_morpho_electro_metadata_dfs()
    """
    #dataframe_morpho = allensdk.get_morpho_dataframe(query_filters)
    #morpho_reconstructions = allensdk.get_morpho_reconstructions(query_filters)
    #dataframe_electro = allensdk.get_electro_dataframe(query_filters)
    #stimuli_data = allensdk.get_electro_stimuli(query_filters)


    dataset_morpho = biophys_datasets.DatasetMorpho({}, "morpho", request.session.session_key, app_name=settings.BIOPHYSICAL_MODELS_APP_NAME)
    dataset_electro = biophys_datasets.DatasetElectro({}, "electro", request.session.session_key, app_name=settings.BIOPHYSICAL_MODELS_APP_NAME)
    dataset_morpho.load(morpho_df)
    dataset_electro.load(electro_df)

    datasets_user = datasets_user_helper.DatasetsUser(settings.BIOPHYSICAL_MODELS_APP_NAME)
    datasets_user.add([dataset_morpho, dataset_electro])
    datasets_user.save(request.session)

    return render(request, 'create_biophys_models.html', data)
    """

    return 0

def load_biophys_models():
    allensdk = biophys_io.AllenSDK(randint(0, 1000))

    biophys_models_folder = allensdk.get_biophys_models()

    return biophys_models_folder

def run_biophys_models():
    allensdk = biophys_io.AllenSDK(empty=True)

    biophys_models_folder = allensdk.run_NEURON_simulation()

    return biophys_models_folder


if __name__ == '__main__':
    allensdk = biophys_io.AllenSDK(empty=True)

    allensdk.get_biophys_simulation_sweeps()
    allensdk.get_all_biophys_models_electro_features()

    print("DONE")