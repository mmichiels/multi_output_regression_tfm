### **Installation:**

##### Install Conda (skip this step if you already have conda installed in your system)

```
cd / && \
wget https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
/bin/bash ~/miniconda.sh -b -p /opt/conda && \
rm ~/miniconda.sh && \
/opt/conda/bin/conda clean -tipsy && \
ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
export PATH=/opt/conda/bin:$PATH
```


##### Install packages and deploy the new conda virtual environment

```
cd multi_output_regression_tfm
conda env create python=3.6.4 -f ./conda_environment.yml
conda activate conda_py364_tfm
```

To support "farthest" (-1) sections in hoc models (in BluePyOpt), edit line 97 of locations.py in the BluePyOpt package to add:
```
elif self.sec_index == -1:
    self.sec_index = iseclist_size-1
```

To install Geppetto (NEURON-UI) run the following additional command:
```
python ./neuron_ui/NEURON-UI/utilities/my_install.py
```

##### Update existing conda environment
Only just in case you already installed this package. In order to
update it, run:

```
cd multi_output_regression_tfm
git pull
conda env update -f ./conda_environment.yml
```

### Running instructions:

Due to the excessive size of the input and output data, 
it is stored in a separate Google Drive repository that it's necessary to download to run the code:

Input-output data set: https://drive.google.com/drive/folders/16CNzyA465prPIMBIs59AtwV-K4Wzpl74?usp=sharing

The *specimen_files* and *new_morpho_reconstructions* folder are only necessary to run the biophysical model optimization with BluePyOpt.
You can also ommit the specimen_files folder download if you only want to run the inference in the pretrained model with new data.

##### To run the pretrained model in a new set of morphologies:

Set the following flags in the ```./helpers/machine_learning/neural_networks/training_one_gaussian.py``` file:
```
inference_only = True
l_measure_data = True
only_electro_configs_without_apic = False
```

| Flag  | Description |
| ------------- | ------------- |
| inference_only  | True: using the pretrained model with new data. False: training a new model with new data.  |
| l_measure_data  | True: only using the l_measure data for training/inference (e.g. NeuroMorpho neurons data). False: using the Allen Cell Types metadata. |
| only_electro_configs_without_apic  | True: ignore the sweeps involving apical dendrites.  |
| train_and_test  | True: split the data set in training-test subsets to validate the score of the learned model in the test set. False: use all the data to train a new model.|

If **l_measure** is True, move and rename your data set csv to 
```./helpers/input_data/l_measure_neurosuites.csv```
If **l_measure** is False, move and rename your data set csv to 
```preprocessed_morpho_biophys_models```

Run:
```
python ./helpers/machine_learning/neural_networks/training_one_gaussian.py
```

##### To train a new model with a new set of morphologies and electrophysiology data for every pair of neurons:
*Download the L-Measure data set for a new set of neurons:*

To to this, navigate to 
https://neurosuites.com/morpho/select_neuromorpho_neurons. It will download the desired neurons from the http://NeuroMorpho.org database.

Once the neurons have been selected, go to https://neurosuites.com/morpho/morpho_data_stats_continuous if you want 
to download the L-Measure data for all the neurons calculated by NeuroMorpho.org. Otherwise, if you want to download the L-Measure
data set calculated by NeuroSuites, go to https://neurosuites.com/morpho/morpho_l_measure.

In the new table, click on the CSV button to download the data set and then remove the last column (Analyze neuron).
Rename this file as "l_measure_neurosuites.csv" and move it to the ./input_data folder.

*Download the morphologies reconstructions in swc format:*

In the same NeuroSuites session, go to https://neurosuites.com/morpho/morpho_download_your_data and 
click in "Download original NeuroMorpho SWC files".

Extract the zip file and move the files to the ./input_data/new_morpho_reconstructions (create it if it doesn't exist).

##### To retrain the original model:
Set the following flags in the ```./helpers/machine_learning/neural_networks/training_one_gaussian.py``` file:
```
inference_only = False
l_measure_data = False
only_electro_configs_without_apic = False #Or True if desired
train_and_test = True
```

#####  To build the biophysical model of the predicted electrophysiology features (BluePyOpt):
You must run first the model inference in the section above.

If the neurons are from the Allen Cell Types database, edit the ```helpers/analysis/bluepyopt_l5pc/main.py``` file and set:
```
new_inference_data = False
```

Otherwise, if neurons are from a new data set, set **new_inference_data** to True.

Run:
```
./helpers/analysis/bluepyopt_l5pc/main.py
```

#####  To run Geppetto (NEURON-UI):
You must run first the biophysical model generation (optimization) in the section above.

To setup a new neuron biophysical model for Geppetto, an utility function has been created to convert the BluePyOpt 
biophysical model to the Geppetto biopyhsical model format. This function is automatically executed when running the section above.

It setups a new biophysical model for Geppetto for every model that has been optimized.
 
Warning: models are overwritten as only one model is configured to run in the Geppetto UI. So in order to build the model
for a specific neuron, be sure that your data set has only the one instance for that neuron, or include an if statement
before the ```convert_model_to_geppetto``` function in the ```helpers/analysis/bluepyopt_l5pc/main.py``` to create the
model only for that specific neuron.

Before running Geppetto, be sure you are in the proper Conda environment and the compiled files (the corresponding ones 
to the BluePyOpt optmization l5 pyramidal cell model) are in the root project folder (there is a "./x86_64" folder).

Run the following command:
```
./neuron_ui/NEURON-UI/NEURON-UI
```
Then open a browser tab in http://localhost:8888/

In the "Sample neurons models" panel select "Custom neuron".
Now to simulate a patch clamp:
- In the upper left panel click in "Point Process", then click on the neuron morphology part that you want
to inject the current in. Set the desired parameters (e.g. delay: 0, duration: 500, amplitude: 0.9)
and click save.
- In the upper left panel click in "Space plot", then click in the neuron morphology part that you want
to record the current from. 
- In the upper left panel click in "Run control" and then click in "Init & Run". The simulation will begin and
after the simulation duration, the the electrophysiology sweeps plot results will come up.
- In the upper right panel you can click in "Results" and enable "Apply voltage colouring to morphologies" and click in 
"Play step by step" to view more clearly what is happening in the simulation.

If you want to change the internal model configuration in the Geppeto UI,
edit the ```./neuron_ui/NEURON-UI/neuron_ui/PTcell.py```. Your can run/debug the model code by running that file
with ```python ./neuron_ui/NEURON-UI/neuron_ui/PTcell.py```.